alphas  = [1/4,1/4,1/4,1/4]; taus  = sqrt([1,1;1,1;1,1;1,1]);
mus     = [1.37,1.36;1.35,-1.36;-1.35,1.36;-1.37,-1.36]; [K,d] = size(mus);
r0      = chi2inv(0.60,d); r = repmat(r0,1,K);
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; del_bar = 1-sqrt(1-del);
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 

figure
h = suptitle('Histograms of data with different MCR levels');
set(h,'FontSize',20,'FontWeight','bold')

mus     = [1.37,1.36;1.35,-1.36;-1.35,1.36;-1.37,-1.36];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,1)
figure(1)
ksdensity(xi,x);
title('0% MCR')

mus     = [1.3,1.26;1.28,-1.26;-1.28,1.26;-1.3,-1.26];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,2)
figure(2)
ksdensity(xi,x);
title('1% MCR')

mus     = [1.25,1.23;1.23,-1.23;-1.23,1.23;-1.25,-1.23];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,3)
figure(3)
ksdensity(xi,x);
title('2% MCR')

mus     = [1.22,1.19;1.20,-1.19;-1.20,1.19;-1.22,-1.19];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,4)
figure(4)
ksdensity(xi,x);
title('3% MCR')

mus     = [1.2,1.15;1.18,-1.15;-1.18,1.15;-1.2,-1.15];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,5)
figure(5)
ksdensity(xi,x);
title('4% MCR')

mus     = [1.15,1.14;1.13,-1.14;-1.13,1.14;-1.15,-1.14];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,6)
figure(6)
ksdensity(xi,x);
title('5% MCR')
