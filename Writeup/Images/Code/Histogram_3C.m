mus = [7.3,0,-7.3]; taus = [2.2,2.2,2.2]; r = [3.6,3.6,3.6]; alphas = [1/3,1/3,1/3]; nu = 100; del = 0.1;
sigs = Comp_True_Sigmas(mus,taus,r,1);
M = Compute_M_v2(alphas,mus,taus,r,del,1);

mus  = [7.3,0,-7.3]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;

figure
h = suptitle('Histograms of data with different MCR levels');
set(h,'FontSize',20,'FontWeight','bold')
subplot(3,2,1)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('0% MCR')

mus  = [6.925,0,-6.925]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,2)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('1% MCR')

mus = [6.68,0,-6.68]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,3)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('2% MCR')

mus = [6.45,0,-6.45];
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,4)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('3% MCR')

mus = [6.238,0,-6.238]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,5)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('4% MCR')

mus = [6.05,0,-6.05]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,6)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('5% MCR')
