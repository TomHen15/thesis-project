alphas  = [1/4,1/4,1/4,1/4]; taus  = sqrt([1,1;1,1;1,1;1,1]);
mus     = [1.37,1.36;1.35,-1.36;-1.35,1.36;-1.37,-1.36]; [K,d] = size(mus);
r0      = chi2inv(0.60,d); r = repmat(r0,1,K);
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; del_bar = 1-sqrt(1-del);
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 

mus     = [2,2;1.95,-2;-1.95,2;-2,-2];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,2)
figure(2)
ksdensity(xi,x);
title('0% MCR (Far)')

mus     = [1.75,1.75;1.73,-1.75;-1.73,1.75;-1.75,-1.75];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,3)
figure(3)
ksdensity(xi,x);
title('0% MCR (Medium)')

mus     = [1.35,1.35;1.34,-1.35;-1.34,1.35;-1.35,-1.35];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,4)
figure(4)
ksdensity(xi,x);
title('0% MCR (Near)')
