mus = [15,7,0,-8]; taus = [2.2,2.2,2.2,2.2]; r = [3.6,3.6,3.6,3.6]; alphas = [1/4,1/4,1/4,1/4]; nu = 100; del = 0.1;
sigs = Comp_True_Sigmas(mus,taus,r,1);
M = Compute_M_v2(alphas,mus,taus,r,del,1);

mus = [15,7.5,0,-7.5];
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;

figure
h = suptitle('Histograms of data with different MCR levels');
set(h,'FontSize',20,'FontWeight','bold')
subplot(3,2,1)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('0% MCR')

mus = [13.95,7,0,-6.95]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,2)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('1% MCR')

mus = [13.5,6.75,0,-6.75]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,3)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('2% MCR')

mus = [13.2,6.75,0,-6.45];
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,4)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('3% MCR')

mus = [12.9,6.7,0,-6.2]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,5)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('4% MCR')

mus = [12.6,6.7,0,-5.9]; 
samp = Sample_TNM(M,alphas,mus,taus,r);     
xi   = samp.mix;
subplot(3,2,6)
histogram(xi,'BinLimits',[min(xi),max(xi)],'Normalization','probability')
title('5% MCR')
