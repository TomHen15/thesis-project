alphas  = [1/3,1/3,1/3]; mus = [1.7,1.8;0,-0.5;-1.7,1.8]; taus  = sqrt([1,1;1,1;1,1]); [K,d] = size(mus);
r0      = chi2inv(0.6,d); r = [r0,r0,r0]; 
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; N = 6;
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 

figure
h = suptitle('Histograms of data with different MCR levels');
set(h,'FontSize',20,'FontWeight','bold')

mus = [1.7,1.8;0,-0.5;-1.7,1.8];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,1)
figure(1)
ksdensity(xi,x);
title('0% MCR')

mus = [1.37,1.6;0,-0.5;-1.37,1.6];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,2)
figure(2)
ksdensity(xi,x);
title('1% MCR')

mus = [1.27,1.57;0,-0.5;-1.27,1.57];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,3)
figure(3)
ksdensity(xi,x);
title('2% MCR')

mus = [1.24,1.52;0,-0.5;-1.24,1.52];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,4)
figure(4)
ksdensity(xi,x);
title('3% MCR')

mus = [1.17,1.51;0,-0.5;-1.17,1.51];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,5)
figure(5)
ksdensity(xi,x);
title('4% MCR')

mus = [1.123,1.5;0,-0.5;-1.123,1.5];
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
%subplot(3,2,6)
figure(6)
ksdensity(xi,x);
title('5% MCR')
