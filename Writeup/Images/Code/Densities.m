mus = [7.3,0,-7.3]; taus = [2.2,2.2,2.2]; r = [3.6,3.6,3.6]; alphas = [1/3,1/3,1/3]; nu = 100; del = 0.1;
sigs = Comp_True_Sigmas(mus,taus,r,1);
M = 500000;

samp = Sample_TNM(M,alphas,mus,taus,r);
xi   = samp.mix;

figure(1)
x = linspace(min(xi),max(xi),10000);
ksdensity(xi,x);

m = mean(xi);
s = std(xi);
r2 = range(xi)/2;
samp2 = Sample_TNM(M,1,m,s,r2);
xi2  = samp2.mix;
x = linspace(min(xi2),max(xi2),10000);

hold on
ksdensity(xi2,x);

samp3 = Sample_TNM(M,1,m,2*s,r2);
xi3  = samp3.mix;
x = linspace(min(xi3),max(xi3),10000);

hold on
ksdensity(xi3,x);
