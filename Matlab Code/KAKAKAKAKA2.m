%%
alphas  = [1/4,1/4,1/4,1/4]; taus  = sqrt([1,1;1,1;1,1;1,1]);
mus     = [1.18,1.13;...
           1.13,-1.13;...
          -1.13,1.13;...
          -1.18,-1.13];
[K,d]   = size(mus);
r0      = chi2inv(0.60,d); r = repmat(r0,1,K);
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; del_bar = 1-sqrt(1-del);
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;

meh     = KmeansPP_DC(xi,K,1);
100*mean(meh.labels~=samp.labels)

%%
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
figure(1)
ksdensity(xi,x);


%%
meh     = KmeansPP_DC(xi,K,1);
100*mean(meh.labels~=samp.labels)

meh2    = MVTNM_EM(samp,meh,K,r,nu);
100*mean(meh2.labels~=samp.labels)

meh3   = MVTNM_EM_EstRadii(samp,meh,K,nu,50);
100*mean(meh3.labels~=samp.labels)

[alphas_bar,betas_bar,~,~]  = Comp_DRSP_Parameters(samp.comps,mus,sigs,M/K,del_bar);
CR_Res  = Test_CR_MV(mus,sigs,meh.mus,meh.sigs,alphas_bar,betas_bar)
CR_Res2 = Test_CR_MV(mus,sigs,meh2.mus,meh2.sigs,alphas_bar,betas_bar)
CR_Res3 = Test_CR_MV(mus,sigs,meh3.mus,meh3.sigs,alphas_bar,betas_bar)
