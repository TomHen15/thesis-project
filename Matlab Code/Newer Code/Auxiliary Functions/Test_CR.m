function out = Test_CR(mus,sigs,mus_hat,sigs_hat,alphas_bar,betas_bar)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests whether the confidence region constraints are satisfied 
% Input:
%   mus         = True means of mixture components
%   sigs        = True standard deviations of mixture components
%   mus_hat     = Estimators for means of mixture components
%   sigs_hat    = Estimators for standard deviations of mixture components
%   alphas_bar  = alpha_bar parameters for bounds of each component
%   betas_bar   = beta_bar parameters for bounds of each component
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,K] = size(mus_hat);

lower_coeff = zeros(1,K);
upper_coeff = zeros(1,K);
lower_bound = zeros(1,K);
upper_bound = zeros(1,K);
mu_criteria = zeros(1,K);
sig_criteria_lower = zeros(1,K);
sig_criteria_upper = zeros(1,K);
for k=1:K
    mu_criteria(k)          = 1/sigs(k)^2*(mus_hat(k)-mus(k))^2 <= betas_bar(k); 
    lower_coeff(k)          = 1/(1+alphas_bar(k));                
    lower_bound(k)          = lower_coeff(k)*sigs_hat(k)^2;
    upper_coeff(k)          = 1/(1-alphas_bar(k)-betas_bar(k));  
    upper_bound(k)          = upper_coeff(k)*sigs_hat(k)^2;
    sig_criteria_lower(k)   = lower_bound(k) <= sigs(k)^2; 
    sig_criteria_upper(k)   = sigs(k)^2 <= upper_bound(k);
end

CR_res = 0;
if (sum(mu_criteria) + sum(sig_criteria_lower) + sum(sig_criteria_upper)) == 3*K
    CR_res = 1;
end

out = struct();
out.CR_res          = CR_res;                                   % indicates whether all the CR criteria are satisfied
out.mu_res          = mu_criteria;                              % indicates whether the mu bound is satisfied
out.sig_res_lower   = sig_criteria_lower;                       % indicates whether the lower sigma bound is satisfied 
out.sig_res_upper   = sig_criteria_upper;                       % indicates whether the upper sigma bound is satisfied 
out.sig_res         = sig_criteria_lower.*sig_criteria_upper;   % indicates whether both sigma bounds are satisfied
out.sig_lower_bound = lower_bound;                              % value of sigma lower bound
out.sig_upper_bound = upper_bound;                              % value of sigma upper bound

end
