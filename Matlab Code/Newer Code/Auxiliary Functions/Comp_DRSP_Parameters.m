function [alpha_bar,beta_bar,gamma1_bar,gamma2_bar] = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M,del_bar)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes the parameter for the confidence bounds and DRSP optimization
% Input:
%   etas         = Cell array of mixture components (or clustered estimators)
%   mus_hat      = estimators for mixture means 
%   sigs_hat     = estimators for mixture standard deviations 
%   M            = estimate for required sample size
%   del_bar      = probability parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,K]      = size(etas);
[~,d]      = size(etas{1});
alpha_bar  = zeros(1,K);  
beta_bar   = zeros(1,K);
gamma1_bar = zeros(1,K);
gamma2_bar = zeros(1,K); 
for k=1:K
    if d==1
        R_hat = max(abs((etas{k}-mus_hat(k))./sigs_hat(k)));  
    else 
        z     = (etas{k}-mus_hat(k,:))./sigs_hat(k,:);
        R_hat = max(sqrt(dot(z,z,2)));
    end
    R_bar         = (1-(R_hat^2+2)*(2+sqrt(2*log(4/del_bar)))/sqrt(M))^(-1/2)*R_hat;
    alpha_bar(k)  = R_bar^2/sqrt(M)*(sqrt(1-d/R_bar^4)+sqrt(log(4/del_bar)));
    beta_bar(k)   = R_bar^2/M*(2+sqrt(2*log(2/del_bar)))^2;
    gamma1_bar(k) = beta_bar(k)/(1-alpha_bar(k)-beta_bar(k));
    gamma2_bar(k) = (1+beta_bar(k))/(1-alpha_bar(k)-beta_bar(k));
end
end