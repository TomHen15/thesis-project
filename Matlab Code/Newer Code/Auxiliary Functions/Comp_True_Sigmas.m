function sigs = Comp_True_Sigmas(mus,taus,r,d)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes standard deviations of the truncated Normal mixture
%   mus     = means of underlying normal components
%   tau     = standard deviations of underlying normal components 
%   r       = truncation radii for mixture components
%   d       = dimension of problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if  d==1
    low     = mus-r;
    high    = mus+r;
    alpha   = (low-mus)./taus;
    beta    = (high-mus)./taus;
    Z       = normcdf(beta)-normcdf(alpha);
    sigs    = sqrt(taus.^2.*(1+(alpha.*normpdf(alpha)-beta.*normpdf(beta))./Z-((normpdf(alpha)-normpdf(beta)./Z)).^2));
else
    [K,d] = size(mus);
    sigs  = zeros(K,d);
    for k=1:K
        fun = @(z) 1/sqrt(2*pi)/chi2cdf(r(k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r(k)-z.^2,d-1);
        C   = integral(fun,-sqrt(r(k)),sqrt(r(k)));
        sigs(k,:) = sqrt(C)*taus(k,:);
    end
end
