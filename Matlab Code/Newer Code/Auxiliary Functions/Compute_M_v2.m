function M = Compute_M_v2(alphas,mus,taus,r,del,d)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the required number of samples for the DRSP problem
% This includes adjustment due to needing to increase sample size 
% as function of the weights of the mixture componen
% Input:
%   alphas  = mixture weights
%   mus     = mixture components mu parameters
%   taus    = mixture components sigma parameters
%   r       = mixture components truncation radii
%   del     = probability parameter
%   d       = dimension of problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,K]   = size(alphas); 
del_bar = 1-sqrt(1-del);

if d==1 % for the univariate case with truncation on [mu-r,mu+r]
    sigs = Comp_True_Sigmas(mus,taus,r,1);
    R    = r./sigs;
else                 % for the multivariate case with level-set truncation
    for k=1:K
        fun  = @(z) 1/sqrt(2*pi)/chi2cdf(r(k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r(k)-z.^2,d-1);
        C    = integral(fun,-sqrt(r(k)),sqrt(r(k)));
        R(k) = sqrt(r(k)/C);
    end 
end

for k=1:K
    temp1 = (R(k)^2+2)^2*(2+sqrt(2*log(4/del_bar)))^2;
    temp2 = (8+sqrt(32*log(4/del_bar)))^2/(sqrt(R(k)+4)-R(k))^4;
    Ms(k) = 1/alphas(k)*ceil(max(temp1,temp2));
end
M = max(Ms);
end 