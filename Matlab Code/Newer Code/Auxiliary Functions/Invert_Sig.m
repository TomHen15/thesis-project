function taus = Invert_Sig(sigs,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    f = @(x) (1./x).^2.*(1-(2.*x.*normpdf(x))./(normcdf(x)-normcdf(-x)));
    xx = linspace(0.1,10,1000000);
    yy = f(xx);

    r_bar = (sigs./r).^2;
    x = interp1(yy,xx,r_bar);
    taus   = r./x;
end