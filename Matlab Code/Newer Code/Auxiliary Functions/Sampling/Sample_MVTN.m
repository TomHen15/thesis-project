function out = Sample_MVTN(M,mu,tau,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate data from multivariate truncated normal with levelset truncation
% Input:
%   M       = number of samples
%   mu      = mean vector for underlying MVN
%   tau     = covariance matrix for underlying MVN
%   r       = truncation parameter for level-set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d]       = size(mu);
ratio       = 1/chi2cdf(r,d);
M2          = floor(M*ratio*1.05);
xi          = mvnrnd(mu,tau,M2);
indicator   = (dot((xi-mu),(tau\(xi-mu)')',2)<=r);
index       = find(indicator);
index       = index(1:M);
out         = xi(index,:);
    
end 
