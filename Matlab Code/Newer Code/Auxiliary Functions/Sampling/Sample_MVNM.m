function out = Sample_MVNM(M,alphas,mus,sigs)
% sample from standard MV normal mixture
[~,d] = size(mus(1)); %dimension
[~,K] = size(alphas);
etas = cell(d);
for k=1:K
    etas{k} = mvnrnd(mus{k},sigs{k},M);
end 
z = mnrnd(1,alphas,M);
[~,Y] = max(z,[],2);

mix = zeros(M,d);
for k=1:K
   mix = mix + z(:,k).*etas{k};
end
out = struct();
out.mix = mix;
out.comps = etas;
out.labels = Y;
end 
