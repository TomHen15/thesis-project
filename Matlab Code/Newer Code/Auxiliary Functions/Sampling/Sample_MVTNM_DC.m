function out = Sample_MVTNM_DC(M,alphas,mus,taus,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate data from mixture of K univarate trcunated normals.
% This script is used when working with diagonal covariances.
% Input:
%   M       = number of samples
%   K       = number of components
%   alphas  = mixture weights
%   mu      = matrix whose rows are mu vector parameters of components
%   tau     = matrix of tau parameters of MVTN with diagonal covariances
%             The row are the standard deviations of the underlying
%             normals in each component.
%   r       = vector of truncation parameters of components
% Output:
%   cell array containing the following
%    - cell array of  mixture components matrices
%    - matrix of mixure samples
%    - vector of component labels 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d] = size(mus); %dimension
[~,K] = size(alphas);
etas = cell(1,K);
for k=1:K
    etas{k} = Sample_MVTN(M,mus(k,:),diag(taus(k,:).^2),r(k));
end 
z = mnrnd(1,alphas,M);
[~,Y] = max(z,[],2);

mix = zeros(M,d);
for k=1:K
   mix = mix + z(:,k).*etas{k};
end
out = struct();
out.mix = mix;
out.comps = etas;
out.labels = Y;
end 
