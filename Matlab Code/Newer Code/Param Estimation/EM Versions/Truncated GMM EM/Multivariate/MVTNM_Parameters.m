function out = MVTNM_Parameters(xi,posteriors,taus_old,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the parameter update for TNM-EM iteration
% Input: 
%   xi          = mixture data (column vector)
%   posteriors  = posterior values 
%   taus_old    = previous tau value (for optimization initialization)
%   r           = truncation radii for components
%   nu          = smoothing parameter for indicators
% Output:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,d]          = size(xi);
[N,K]          = size(posteriors);
SumR           = sum(posteriors,1);

% compute new alpha parameters
alphas_hat     = SumR./N;

% compute new mu parameters
mus_hat        = zeros(K,d);
for k=1:K
    mus_hat(k,:) = sum(posteriors(:,k).*xi)/SumR(k);
end

% compute new tau parameters
options        = optimoptions('fminunc','Algorithm','quasi-newton','FiniteDifferenceType','central',...
                             'MaxFunctionEvaluations',10000,'MaxIterations',10000,...
                             'display','off');
initvals       = taus_old;
fun            = @(arg) MVTNM_DC_NLL(alphas_hat,mus_hat,arg,r,xi,nu);
[taus_hat,~]   = fminunc(fun,initvals,options);

% output results
out            = struct();
out.alphas     = alphas_hat;
out.mus        = mus_hat;
out.taus       = taus_hat;

end
        
