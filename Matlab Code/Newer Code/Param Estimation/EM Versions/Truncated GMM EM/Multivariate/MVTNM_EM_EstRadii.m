function out = MVTNM_EM_EstRadii(samp,init,K,nu,maxit)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform EM for the truncated normal mixture with radii estimation
% Input:
%   samp        = sample data from mixture (including labels)
%   init        = initialization values (obtained from KMeans++)
%   K           = number of components
%   nu          = smoothing parameter
%   maxit       = maximal number of iterations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xi              = samp.mix;
mus_hat(:,:,1)  = init.mus;
sigs_hat(:,:,1) = init.sigs;
alphas_hat(1,:) = init.alphas;
Y_hat           = init.labels;

%%% use Kmeans++ initialization to obtain first estimate for r
r_hat(1,:)      = zeros(1,K);
for k=1:K
    ind         = Y_hat == k; 
    etas        = xi(ind,:);
    z           = (etas-mus_hat(k,:,1))./sigs_hat(k,:,1);
    r_hat(1,k)  = max(dot(z,z,2));
end

[K,d]           = size(init.mus); 
C               = zeros(K,1);
for k=1:K 
    fun         = @(z) 1/sqrt(2*pi)/chi2cdf(r_hat(1,k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r_hat(1,k)-z.^2,d-1);
    C(k)        = 1./sqrt(integral(fun,-sqrt(r_hat(1,k)),sqrt(r_hat(1,k))));
end 
taus_hat(:,:,1) = C.*sigs_hat(:,:,1);

Y_hat           = MVTNM_DC_LLClassify(xi,alphas_hat,mus_hat,taus_hat,r_hat,nu);
r_hat(1,:)      = zeros(1,K);
for k=1:K
    ind         = Y_hat == k; 
    etas        = xi(ind,:);
    z           = (etas-mus_hat(k,:,1))./taus_hat(k,:,1);
    r_hat(1,k)  = max(dot(z,z,2));
end

r_old           = r_hat(1,:);
mus_tmp         = mus_hat(:,:,1);
sigs_tmp        = sigs_hat(:,:,1);
taus_tmp        = taus_hat(:,:,1);
iter            = 1;
while (1 && iter<30)
    Y_hat       = MVTNM_DC_LLClassify(xi,alphas_hat,mus_tmp,taus_tmp,r_old,nu);
    for k=1:K
        ind             = Y_hat == k; 
        etas            = xi(ind,:);
        mus_tmp(k,:)    = mean(etas);
        sigs_tmp(k,:)   = sqrt(diag(cov(etas)));
        fun             = @(z) 1/sqrt(2*pi)/chi2cdf(r_old(k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r_old(k)-z.^2,d-1);
        C(k)            = 1./sqrt(integral(fun,-sqrt(r_old(k)),sqrt(r_old(k))));
        taus_tmp(k,:)   = C(k).*sigs_tmp(k,:);
        z               = (etas-mus_tmp(k,:))./taus_tmp(k,:);
        r_new(k)        = max(dot(z,z,2));
    end
    if max(abs(r_new-r_old)) < 10^-3
        break
    end
    iter       = iter+1;
    r_old      = r_new;
end
r_hat          = r_new;

%%% compute initial complete log-likelihood value and iterate EM
LLval           = -MVTNM_DC_NLL(alphas_hat(1,:),mus_hat(:,:,1),taus_hat(:,:,1),r_hat(1,:),xi,nu);
LL_vals         = LLval; %save the LL values

% start iterative process for successive optimization
stop = 1; i=1;
while (stop && i <= maxit)   
    % Expectation step
    posteriors      = MVTNM_Posterior(xi,alphas_hat(i,:),mus_hat(:,:,i),taus_hat(:,:,i),r_hat(i,:),nu);

    % Maximimization step
    params          = MVTNM_Parameters(xi,posteriors,taus_hat(:,:,i),r_hat(i,:),nu);
    i               = i+1; 
    alphas_hat(i,:) = params.alphas;
    mus_hat(:,:,i)  = params.mus;
    taus_hat(:,:,i) = params.taus;

    % optimize for r with the new updates for the other parameters
    posteriors = MVTNM_Posterior(xi,alphas_hat(i,:),mus_hat(:,:,i),taus_hat(:,:,i),r_hat(i-1,:),nu);
    [~,Y_hat]  = max(posteriors,[],2);
    for k=1:K
        ind             = Y_hat == k; 
        etas            = xi(ind,:);
        sigs_hat(k,:,i) = sqrt(diag(cov(etas)));
        fun             = @(z) 1/sqrt(2*pi)/chi2cdf(r_hat(i-1,k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r_hat(i-1,k)-z.^2,d-1);
        C(k)            = 1./sqrt(integral(fun,-sqrt(r_hat(i-1,k)),sqrt(r_hat(i-1,k))));
        taus_hat(k,:,i) = C(k).*sigs_hat(k,:,i);
        z               = (etas-mus_hat(k,:,i))./taus_hat(k,:,i);
        r_hat(i,k)      = max(dot(z,z,2));
    end

    % Evaluate log-likelihood
    LLval           = -MVTNM_DC_NLL(alphas_hat(i,:),mus_hat(:,:,i),taus_hat(:,:,i),r_hat(i,:),xi,nu);
    diff(i)         = abs(LLval-LL_vals(length(LL_vals)));
    %disp(diff(i))
    if diff(i) < 10^-4
       stop         = 0;
    end
    LL_vals         = [LL_vals,LLval];
end

i                   = length(mus_hat); iter = i;
posteriors          = MVTNM_Posterior(xi,alphas_hat(i,:),mus_hat(:,:,i),taus_hat(:,:,i),r_hat(i,:),nu);
[~,assignments]     = max(posteriors,[],2);

% this sorts the output and labels to fit the order of the true parameters 
labels              = zeros(length(xi),1);
mus_hat2            = mus_hat(:,:,i); 
mus_hat3            = mus_hat2; 
taus_hat2           = taus_hat(:,:,i);
sigs_hat2           = sigs_hat(:,:,i);
alphas_hat2         = alphas_hat(i,:);
r_hat2              = r_hat(i,:);
for i=1:K
    [~,i1]          = max(mus_hat2(:,1));
    alphas_out(i)   = alphas_hat2(i1);
    mus_out(i,:)    = mus_hat2(i1,:);
    taus_out(i,:)   = taus_hat2(i1,:);
    sigs_out(i,:)   = sigs_hat2(i1,:);
    r_out(i)        = r_hat2(i1);
    l = find(mus_hat3(:,1) == mus_out(i,1),1);
    labels(assignments==l) = i;
    sigs_hat2(i1,:) = [];
    taus_hat2(i1,:) = [];
    mus_hat2(i1,:)  = [];
    alphas_hat2(i1) = [];
    r_hat2(i1)      = [];
end

out                 = struct();
out.LLvals          = LL_vals;
out.log.mus         = mus_hat;
out.log.taus        = taus_hat;
out.log.sigs        = sigs_hat;
out.log.radii       = r_hat;
out.alphas          = alphas_out;
out.mus             = mus_out;
out.taus            = taus_out;
out.sigs            = sigs_out;
out.radii           = r_out;
out.labels          = labels;
out.iter            = iter;
end
    