function out = MVTNM_EM_MisclassRate(M,alphas,mus,taus,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes the parameter estimates by EM and misclassification rate 
% Input: 
%   M       = sample size
%   alphas  = mixture weights
%   mus     = mu parameters of mixture components
%   taus    = tau parameters of mixture components
%   r       = truncation radii of mixture components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[K,d]                   = size(mus);
samp                    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi                      = samp.mix;
Y                       = samp.labels;

Estimators              = MVTNM_EM(samp,K,r,nu);
MCR                     = 100*mean(Y~=Estimators.labels); %misclassification rate
   
out                     = struct();
out.MCR                 = MCR;
out.alphas              = Estimators.alphas;
out.mus                 = Estimators.mus;
out.sigs                = Estimators.sigs;
out.taus                = Estimators.taus;
out.labels              = Estimators.labels;
out.tlabels             = Y;
out.iter                = Estimators.iter;

end 
