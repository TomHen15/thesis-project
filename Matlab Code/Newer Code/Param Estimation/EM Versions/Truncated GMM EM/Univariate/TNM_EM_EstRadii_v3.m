function out = TNM_EM_EstRadii_v3(xi,K,nu,maxit)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform EM for the truncated normal mixture with radii estimation
% Input:
%   xi          = mixture data (column vector)
%   K           = number of components
%   nu          = smoothing parameter
%   maxit       = maximal number of iterations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

options         = optimoptions('fmincon','FiniteDifferenceType','central',...
                       'MaxFunctionEvaluations',10000,'MaxIterations',10000,...
                       'display','off');

%%% Initialize parameters using Kmeans++
init            = KmeansPP(xi,K,1);
alphas_hat      = init.alphas;
mus_hat         = init.mus;
sigs_hat        = init.sigs;
Y_hat           = init.labels;

%%% use Kmeans++ initialization to obtain first estimate for r
r_hat           = zeros(1,K);
for k=1:K
    ind = Y_hat == k; 
    etas        = xi(ind);
    r_hat(k)    = max(abs(etas-mus_hat(k)));
end

% special heuristic for case K>2 to determine central radii
if K>2
    %%% use r_hat and sigs_hat estimate to compute taus_hat 
    taus_hat    = Invert_Sig(sigs_hat,r_hat);

    %%% use posterior classification and then recompute r_hat
    r_old       = r_hat; 
    iter        = 1;
    while (1 && iter < 20)
        posteriors   = TNM_Posterior(xi,alphas_hat,mus_hat,taus_hat,r_old,nu);
        [~,Y_hat]    = max(posteriors,[],2);
        r_new        = zeros(1,K);
        for k=1:K
            ind      = Y_hat == k; 
            etas     = xi(ind);
            r_new(k) = max(abs(etas-mus_hat(k)));
        end
        if max(abs(r_old-r_new)) < 10^-4
            %break
        end
        r_old      = r_new;
        iter       = iter+1;
    end
    r_hat       = r_new; 

    %%% optimize w.r.t to r using the estiamtes for the other parameters
    initvals              = r_hat(2:(K-1));
    fun                   = @(arg) TNM_NLL(alphas_hat,mus_hat,taus_hat,[r_hat(1),arg,r_hat(K)],xi,nu);
    lb                    = r_hat(2:(K-1));
    ub                    = r_hat(2:(K-1))+0.2*sigs_hat(2:(K-1));
    [r_hat(2:(K-1)),~]  = fmincon(fun,initvals,[],[],[],[],lb,ub,[],options);
end

%%% use the radii estimation to optimize other parameters
out                 = TNM_EM(xi,K,r_hat,nu);
out.radii           = r_hat;
end
    