function out = TNM_EM_MisclassRate(M,alphas,mus,taus,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes the parameter estimates by EM and misclassification rate 
% Input: 
%   M       = sample size
%   alphas  = mixture weights
%   mus     = mu parameters of mixture components
%   taus    = tau parameters of mixture components
%   r       = truncation radii of mixture components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,K]                   = size(alphas);
sig_true                = Comp_True_Sigmas(mus,taus,r,1);
samp                    = Sample_TNM(M,alphas,mus,taus,r);
xi                      = samp.mix;
Y                       = samp.labels;
histogram(xi,30,'normalization','probability')

Estimators              = TNM_EM(xi,K,r,nu);
alphas_hat              = Estimators.alphas;
mus_hat                 = Estimators.mus;
sigs_hat                = Estimators.sigs;
Y_hat                   = Estimators.labels;
MCR                     = 100*mean(Y~=Y_hat); %misclassification rate

ALPHAS                  = [alphas;alphas_hat];
MUS                     = [mus;mus_hat];
SIGS                    = [sig_true;sigs_hat];

T                       = table(ALPHAS,MUS,SIGS);
T.Properties.VariableNames  = {'alphas','mus','sigs'};
T.Properties.RowNames       = {'Real Values','Estimators'};
    
out                     = struct();
out.MCR                 = MCR;
out.param               = T;

end 
