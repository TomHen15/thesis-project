function out = TNM_EM(xi,K,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform EM for the truncated normal mixture
% Input:
%   xi          = mixture data (column vector)
%   K           = number of components
%   r           = truncation radii for components 
%   nu          = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[N,~]        = size(xi);

%%% Initialize parameters using Kmeans++
init         = KmeansPP(xi,K,15);
mus_hat      = init.mus;
taus_hat     = init.sigs;
alphas_hat   = init.alphas;

%%% compute initial complete log-likelihood value and iterate EM
LLval        = -TNM_NLL(alphas_hat,mus_hat,taus_hat,r,xi,nu);
LL_vals      = LLval; %save the LL values

stop = 1; iter = 1;
while stop
    %iter
    % Expectation step
    posteriors = TNM_Posterior(xi,alphas_hat,mus_hat,taus_hat,r,nu);

    % Maximimization step
    params     = TNM_Parameters(xi,posteriors,taus_hat,r,nu);
    alphas_hat = params.alphas;
    mus_hat    = params.mus;
    taus_hat   = params.taus;
    
    % Evaluate log-likelihood
    LLval      = -TNM_NLL(alphas_hat,mus_hat,taus_hat,r,xi,nu);
    if abs(LLval-LL_vals(length(LL_vals))) < 10^-4
        stop   = 0;
    end
    LL_vals    = [LL_vals,LLval];
    iter       = iter+1;
end

[~,assignments]     = max(posteriors,[],2); %cluster assignment by maximal posterior

% this sorts the output and labels to fit the order of the true parameters 
labels              = zeros(N,1);
mus_hat2            = mus_hat;
mus_hat3            = mus_hat; 
taus_hat2           = taus_hat;
alphas_hat2         = alphas_hat;
for i=1:K
    [~,i1]          = max(mus_hat2);
    mus_hat(i)      = mus_hat2(i1);
    taus_hat(i)     = taus_hat2(i1);
    alphas_hat(i)   = alphas_hat2(i1);
    l = find(mus_hat3 == mus_hat(i),1);
    labels(assignments==l) = i;
    taus_hat2(i1)   = [];
    mus_hat2(i1)    = [];
    alphas_hat2(i1) = [];
end

% sort output and return
sigs_hat            = Comp_True_Sigmas(mus_hat,taus_hat,r,1);
out                 = struct();
out.alphas          = alphas_hat;
out.mus             = mus_hat;
out.taus            = taus_hat;
out.sigs            = sigs_hat;
out.labels          = labels;
out.LLvals          = LL_vals;
out.iter            = iter;
end