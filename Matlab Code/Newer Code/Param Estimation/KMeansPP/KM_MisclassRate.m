function out = KM_MisclassRate(M,alphas,mus,taus,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes the parameter estimates by KMeansPP and misclassification rate 
% Input: 
%   M       = sample size
%   alphas  = mixture weights
%   mus     = mu parameters for mixture components
%   taus    = tau parameters for mixture components
%   r       = truncation radii for mixture components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,K]                   = size(alphas);
[mu_true,~,sig_true]    = Comp_True_Parameters(mus,taus,r);
samp                    = Sample_TNM(M,alphas,mus,taus,r);
xi                      = samp.mix; 
Y                       = samp.labels;
histogram(xi,30,'normalization','probability')

Estimators              = KmeansPP(xi,K,15);
alphas_hat              = Estimators.alphas;
mus_hat                 = Estimators.mus;
sigs_hat                = Estimators.sigs;
Y_hat                   = Estimators.labels;
MCR                     = 100*mean(Y~=Y_hat); %misclassification rate

ALPHAS                  = [alphas;alphas_hat];
MUS                     = [mu_true;mus_hat];
SIGS                    = [sig_true;sigs_hat];

T                       = table(ALPHAS,MUS,SIGS);
T.Properties.VariableNames  = {'alphas','mus','sigs'};
T.Properties.RowNames       = {'Real Values','Estimators'};

out                     = struct();
out.MCR                 = MCR;
out.param               = T;

end 
