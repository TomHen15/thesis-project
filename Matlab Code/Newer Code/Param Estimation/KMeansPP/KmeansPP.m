function out = KmeansPP(xi,K,itr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: Compute K-means clustering with K clusters
% Input:
%   xi     = data points to be clustered
%   K      = desired number of cluters
%   itr    = number of iterations (deprecated)
% Output:
%   labels = vector of cluster labels 
%   mus    = vector of cluster means 
%   sigs   = vector of cluster standard deviations
%   alphas = vector of cluster weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[N,d] = size(xi);
[assignments,~] = kmeans(xi,K);

% compute probabilities by cluster proportion
counts      = histc(assignments(:),unique(assignments));
priors      = (counts./N)';

if d == 1
    sigs    = zeros(1,K);
    mus     = zeros(1,K);
    alphas  = zeros(1,K);

    % compute means and standard-devs
    mus_temp = zeros(1,K);
    stds = zeros(1,K);
    for k=1:K
        temp = xi(assignments==k,:);
        mus_temp(k) = mean(temp);
        stds(k) = std(temp);
    end

    labels = zeros(N,1);
    %this part sorts the label values
    mus2 = mus_temp;
    for i=1:K
        [~,i1] = max(mus2);
        mus(i) = mus2(i1);
        sigs(i) = stds(i1);
        alphas(i) = priors(i1);
        l = find(mus_temp == mus(i),1);
        labels(assignments==l) = i;
        stds(i1) = [];
        mus2(i1) = [];
        priors(i1) = [];
    end
else
    sigs     = cell(1,K);
    mus      = cell(1,K);
    alphas   = zeros(1,K);
    % compute means and covariances
    mus_temp = cell(1,K);
    covs     = cell(1,K);
    for k=1:K
        temp        = xi(assignments==k,:);
        mus_temp{k} = mean(temp);
        covs{k}     = cov(temp);
    end
    labels  = zeros(N,1);
    %this part sorts the label values
    mus2    = cell2mat(mus_temp');
    for i=1:K
        [~,i1]      = max(mus2(:,1));
        mus{i}      = mus2(i1,:);
        sigs{i}     = covs{i1};
        alphas(i)   = priors(i1);
        meh         = mus{i};
        meh2        = cell2mat(mus_temp');
        l = find(meh2(:,1) == meh(1),1);
        labels(assignments==l) = i;
        covs(i1)    = [];
        mus2(i1,:)  = [];
        priors(i1)  = [];
    end
end
out = struct();
out.mus = mus;
out.sigs = sigs;
out.alphas = alphas;
out.labels = labels;
end