function y = KM_ClustAssign(xi,C)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: Compute cluster assignments for K-means iteration
% Input:
%   xi = data points to be clustered
%   C = current centroids
% Output:
%   y = vector of cluster assignments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dists = pdist2(xi,C);
[~,y] = min(dists,[],2); %take the index of the nearest centroid.

end 

    