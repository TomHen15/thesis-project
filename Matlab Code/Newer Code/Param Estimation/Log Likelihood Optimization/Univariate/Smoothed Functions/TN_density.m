function out = TN_density(mu,tau,r,x,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of truncated normal supported on [mu-r,mu+r]
% This uses continuous surrogate for the interval indicator
% Input:
% mu  = mu parameter of TN
% tau = tau parameter of TN
% r   = truncation radius of TN
% x   = points of evaluation
% nu  = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dens       = normpdf(x,mu,tau);
norm_const = (normcdf(r/tau)-normcdf(-r/tau));
out        = (dens./norm_const.*SmoothInd(mu,r,x,nu))';
end