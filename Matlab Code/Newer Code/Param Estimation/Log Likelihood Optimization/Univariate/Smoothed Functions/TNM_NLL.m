function out = TNM_NLL(alphas,mus,taus,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the negative log-likelihood of mixture of truncated normals
% This uses continuous surrogate for the interval indicators
% Input:
% alphas = mixture weights
% mus    = mu parameters of mixture components
% taus   = tau parameters of mixture components
% r      = truncation radii of mixture components
% xi      = points of evaluation (needs be a column vector)
% nu     = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TNM_dens = TNM_density(alphas,mus,taus,r,xi,nu);
out = -sum(log(TNM_dens));
end