function out = TNM_density(alphas,mus,taus,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of mixture of truncated normals
% This uses continuous surrogate for the interval indicators
% Input:
% alphas = mixture weights
% mus    = mu parameters of mixture components
% taus   = tau parameters of mixture components
% r      = truncation radii of mixture components
% xi      = points of evaluation (needs be a column vector)
% nu     = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,~] = size(xi);
[~,K]  = size(r);   
dens_values = zeros(N,K); % contains the densities of each component at the evaluation points
for k=1:K
    dens_values(:,k) = TN_density(mus(k),taus(k),r(k),xi,nu);
end
out = sum(alphas.*dens_values,2); % contains the density of the mixture at the evaluation points
end 