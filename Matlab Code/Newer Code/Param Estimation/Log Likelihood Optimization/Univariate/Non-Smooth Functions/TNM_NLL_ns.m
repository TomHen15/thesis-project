function out = TNM_NLL_ns(alphas,mus,taus,r,xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the negative log-likelihood of mixture of truncated normals
% This uses continuous surrogate for the interval indicators
% Input:
% alphas = mixture weights
% mus    = mu parameters of mixture components
% taus   = tau parameters of mixture components 
% r      = truncation radii of mixture components
% xi     = mixture data (needs be a column vector)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TNM_dens = TNM_density_ns(alphas,mus,taus,r,xi);
out = -sum(log(TNM_dens));
end