function out = TNM_density_ns(alphas,mus,taus,r,xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of mixture of truncated normals
% Input:
% alphas = mixture weights
% mus    = mu parameters of mixture components
% taus   = tau parameters of mixture components
% r      = truncation radii of mixture components
% xi     = points of evaluation (needs be a column vector)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,~] = size(xi);
[~,K]  = size(r);   
dens_values = zeros(N,K); % contains the densities of each component at the evaluation points

for k=1:K
    dens_values(:,k) = TN_density_ns(mus(k),taus(k),r(k),xi);
end
out = sum(alphas.*dens_values,2);
end 