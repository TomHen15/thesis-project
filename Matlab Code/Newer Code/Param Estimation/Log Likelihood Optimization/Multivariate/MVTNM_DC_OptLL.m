function out = MVTNM_DC_OptLL(samp,init,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Estimators using Kmeans++ and LL optimization
% LL optimization is used only for sigma computation
% Input: 
%   samp    = sample from mixture
%   init    = initialization values from KMeans++
%   r       = truncation radii for mixture components
%   nu      = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xi                  = samp.mix; 
% get initial estimators from Kmeans++
mus_hat             = init.mus;
sigs_init           = init.sigs;
alphas_hat          = init.alphas;

% compute the constant to transform from sigmas to taus
[K,d] = size(init.mus); 
C = zeros(K,1);
for k=1:K
    fun             = @(z) 1/sqrt(2*pi)/chi2cdf(r(k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r(k)-z.^2,d-1);
    C(k)            = 1./sqrt(integral(fun,-sqrt(r(k)),sqrt(r(k))));
end 
taus_init           = C.*sigs_init;


% Compute estimators with LL optimization
% initializaiton is done using the Kmeans++ estimators.
options             = optimoptions('fminunc','Algorithm','quasi-newton','FiniteDifferenceType','central',...
                       'MaxFunctionEvaluations',10000,'MaxIterations',10000,...
                       'display','off');
initvals            = taus_init;
fun                 = @(arg) MVTNM_DC_NLL(alphas_hat,mus_hat,arg,r,xi,nu);
[taus_hat,~]        = fminunc(fun,initvals,options);

% classify and sort the output and labels to fit the order of the true parameters 
assignments         = MVTNM_DC_LLClassify(xi,alphas_hat,mus_hat,taus_hat,r,nu);
labels              = zeros(length(xi),1);
mus_hat2            = mus_hat; 
mus_hat3            = mus_hat; 
taus_hat2           = taus_hat;
alphas_hat2         = alphas_hat;
for i=1:length(mus_hat)
    [~,i1]          = max(mus_hat2(:,1));
    mus_hat(i,:)    = mus_hat2(i1,:);
    taus_hat(i,:)   = taus_hat2(i1,:);
    alphas_hat(i)   = alphas_hat2(i1);
    l = find(mus_hat3(:,1) == mus_hat(i,1),1);
    labels(assignments==l) = i;
    taus_hat2(i1,:) = [];
    mus_hat2(i1,:)  = [];
    alphas_hat2(i1) = [];
end

for k=1:K
    ind = labels == k; 
    etas          = xi(ind,:);
    sigs_hat(k,:) = sqrt(diag(cov(etas)));
    taus_hat(k,:) = C(k).*sigs_hat(k,:);
end

out                 = struct();
out.alphas          = alphas_hat;
out.mus             = mus_hat;
out.taus            = taus_hat;
out.sigs            = sigs_hat;
out.labels          = labels;
out.MCR             = 100*mean(labels~=samp.labels);

end
