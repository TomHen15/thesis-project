function labels = MVTNM_DC_LLClassify(xi,alphas_hat,mus_hat,taus_hat,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Classifies the mixture sample to components based on the LL estimators;
% Input:
%   xi          = mixture sample
%   alphas_hat  = Estimators for mixture weights
%   mus_hat     = Estimators for mu parameters
%   taus_hat    = Estimators for tau parameters 
%   r           = truncation radii for components
%   nu          = smoothing parameter for log-likelihood
% Output:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[M,~]           = size(xi);
[~,K]           = size(alphas_hat);
temp            = zeros(M,K); 

for k=1:K
    temp(:,k)   = alphas_hat(k).*MVTN_DC_density(mus_hat(k,:),taus_hat(k,:),r(k),xi,nu);
end

norm_const      = sum(temp,2);
posteriors      = temp./norm_const;
[~,labels]      = max(posteriors,[],2); 

