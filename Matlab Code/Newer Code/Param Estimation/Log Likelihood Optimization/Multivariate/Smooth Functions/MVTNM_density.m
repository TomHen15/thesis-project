function out = MVTNM_density(alphas,mus,taus,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of mixture of MV truncated normals
% This uses continuous surrogate for the interval indicators
% Input:
%   alphas = mixture weights
%   mu     = cell array of mu vector parameters of components
%   tau    = cell array of matrices of tau parameters of components
%   r      = vector of truncation parameters of components
%   xi     = point of evaluation
%   nu     = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,~] = size(xi);
[~,K]  = size(r);   
dens_values = zeros(N,K); % contains the densities of each component at the evaluation points

for k=1:K
    dens_values(:,k) = MVTN_density(mus{k},taus{k},r(k),xi,nu);
end
out = sum(alphas.*dens_values,2);
end 