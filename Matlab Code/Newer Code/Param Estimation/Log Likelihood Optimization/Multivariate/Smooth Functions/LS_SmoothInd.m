function out = LS_SmoothInd(mu,tau,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Smoothed indicactor on interval level-set of Gaussian
% Input:
% mu    = mean parameter (vector)
% tau   = covariance parameter (matrix)
% r     = truncation parameter
% xi     = points of evaluation
% nu    = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z = (sqrtm(tau)\(xi-mu)')';
a = exp(-nu*(r-dot(z,z,2)+1/sqrt(nu)));
out = 1./(1+a);
end 