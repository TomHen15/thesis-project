function out = MVTNM_NLL(alphas,mus,taus,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the negative log-likelihood of mixture of MV truncated normals
% This uses continuous surrogate for the interval indicators
% Input:
%   alphas = mixture weights
%   mu     = cell array of mu vector parameters of components
%   tau    = cell array of matrices of tau parameters of components
%   r      = vector of truncation parameters of components
%   xi     = points of evaluation 
%   nu     = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TNM_dens = MVTNM_density(alphas,mus,taus,r,xi,nu);
out = -sum(log(TNM_dens));
end