function out = MVTN_density(mu,tau,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of MV truncated normal supported on levelset with truncation parameters r.
% This uses continuous surrogate for the interval indicator
% Input:
%   mu    = vector of mu parameters of MVTN
%   tau   = matrix of tau parameters of MVTN
%   r     = truncation parameter of MVTN
%   xi    = points of evaluation
%   nu  = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d]               = size(mu); % dimension
normal_dens         = mvnpdf(xi,mu,tau);
normalization_const = chi2cdf(r^2,d);
indicator           = LS_SmoothInd(mu,tau,r,xi,nu);
out                 = normal_dens./normalization_const.*indicator;
end