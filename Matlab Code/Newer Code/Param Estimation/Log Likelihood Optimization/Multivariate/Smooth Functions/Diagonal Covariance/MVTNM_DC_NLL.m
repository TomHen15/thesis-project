function out = MVTNM_DC_NLL(alphas,mus,taus,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the negative log-likelihood of mixture of MV truncated normals
% This uses continuous surrogate for the interval indicators
% Input:
%   alphas = mixture weights
%   mu     = matrix whose rows are mu vector parameters of components
%   tau    = matrix of tau parameters of MVTN with diagonal covariances
%             The row are the standard deviations of the underlying
%             normals in each component.
%   r      = vector of truncation parameters of components
%   xi     = point of evaluation
%   nu     = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TNM_dens = MVTNM_DC_density(alphas,mus,taus,r,xi,nu);
out = -sum(log(TNM_dens));
end