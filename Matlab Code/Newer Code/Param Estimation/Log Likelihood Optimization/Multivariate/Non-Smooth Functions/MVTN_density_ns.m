function out = MVTN_density_ns(mu,tau,r,xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of MV truncated normal supported on levelset with truncation parameters r.
% Input:
% mu    = vector of mu parameters of MVTN
% tau   = matrix of tau parameters of MVTN
% r     = truncation parameter of MVTN
% xi    = points of evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d]               = size(mu); % dimension
normal_dens         = mvnpdf(xi,mu,tau);
normalization_const = chi2cdf(r^2,d);
indicator           = (dot((xi-mu),(tau\(xi-mu)')',2)<=r);
out                 = normal_dens./normalization_const.*indicator;
end

