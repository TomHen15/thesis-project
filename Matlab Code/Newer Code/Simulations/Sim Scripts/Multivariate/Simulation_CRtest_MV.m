function out = Simulation_CRtest_MV(N,M,alphas,mus,taus,r,del,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the Estimators and does CR test with various methods of estimation
% The methods are
%   - Use underlying components
%   - Standard GMM EM
%   - Kmeans++ Alone
%   - Kmeans++ followed by optimization of LL for taus.
%   - EM for TNM with known truncation radii
%   - EM for TNM with estimated truncation radii
% Input: 
%   N       = number of simulations
%   M       = sample size
%   alphas  = mixture weights
%   mus     = mu parameters of mixture components
%   taus    = tau parameters of mixture components
%   r       = truncation radii of mixture components
%   del     = probability parameter
%   nu      = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[K,d]                   = size(mus);
del_bar                 = 1 - sqrt(1-del);
%%%% compute true parameter values of truncated normals %%%%
sigs                    = Comp_True_Sigmas(mus,taus,r,d); 
M1                      = Compute_M(alphas,mus,taus,r,del,d);   %used in DRSP parameter computation


%%%% Store all the relevant parameters for output %%%%
params = table(alphas',mus,taus,sigs,r');                       %save parameters
params.Properties.VariableNames = {'alphas','mus','taus','sigs','truncation_radii'};

%%%% define data containers %%%% 
Z0 = zeros(N,K); Z1 = zeros(N,1);
Estimators_nonEM = cell(N,8);                                   %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res)
CR_Data_nonEM = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                    %stores all the CR test results
CR_Data_nonEM.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
CR_SigData_nonEM = cell(N,5);                                   %stores the variance test data (lower_bounds,sigs_squared,upper_bounds,lower_res,upper_res)

Estimators_GMM_EM = cell(N,9);                                  %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res,MCR)
CR_Data_GMM_EM = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                   %stores all the CR test results
CR_Data_GMM_EM.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
CR_SigData_GMM_EM = cell(N,5);                                  %stores the variance test data (lower_bounds,sigs_squared,upper_bounds,lower_res,upper_res)
     
Estimators_KM = cell(N,9);                                      %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res,MCR)
CR_Data_KM = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                       %stores all the CR test resul
CR_Data_KM.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
CR_SigData_KM = cell(N,5);                                      %stores the variance test data (lower_bounds,sigs_squared,upper_bounds,lower_res,upper_res)    

Estimators_KM_LL = cell(N,9);                                   %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res,MCR)
CR_Data_KM_LL = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                    %stores all the CR test results
CR_Data_KM_LL.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
CR_SigData_KM_LL = cell(N,5);                                    %stores the variance test data (lower_bounds,sigs_squared,upper_bounds,lower_res,upper_res)   

Estimators_TNM_EM = cell(N,9);                                  %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res,MCR)
CR_Data_TNM_EM = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                   %stores all the CR test results
CR_Data_TNM_EM.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
CR_SigData_TNM_EM = cell(N,5);                                  %stores the variance test data (lower_bounds,sigs_squared,upper_bounds,lower_res,upper_res)
     
Estimators_TNM_EM_EstRadii = cell(N,10);                        %stores the estimators and the CR results (alphas,mus,sigs,radii,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res,MCR)
CR_Data_TNM_EM_EstRadii = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);          %stores all the CR test results
CR_Data_TNM_EM_EstRadii.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
CR_SigData_TNM_EM_EstRadii = cell(N,5);                         %stores the variance test data (lower_bounds,sigs_squared,upper_bounds,lower_res,upper_res)
     
%%%% perform simulation %%%%
parfor_progress(N);
parfor i = 1:N 
    %%%% generate the sample %%%%
    while 1
        samp                         = Sample_MVTNM_DC(M,alphas,mus,taus,r); 
        xi                           = samp.mix;
        Y                            = samp.labels;
        out_KM                       = KmeansPP_DC(xi,K,1);
        MCR                          = 100*mean(Y~=out_KM.labels);
        if MCR < 15
            break
        end
    end

    %%% Estimate with underlying components %%%%
    mus_hat                          = cellfun(@mean,samp.comps,'UniformOutput',0);
    mus_hat                          = reshape(cell2mat(mus_hat'),K,d);
    sigs_hat                         = cellfun(@(X) sqrt(diag(cov(X))),samp.comps,'UniformOutput',0);
    sigs_hat                         = reshape(cell2mat(sigs_hat),d,K)';
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    sigs_res_all                     = prod(sigs_res_comp);
    Estimators_nonEM(i,:)            = {alphas',mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,sigs_res_all,CR_res.CR_res};  
    CR_Data_nonEM(i,:)               = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};                   
    CR_SigData_nonEM(i,:)            = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,CR_res.sig_res_lower,CR_res.sig_res_upper};              
      
    %%% Estimate with GMM EM %%%%
    out_GMM_EM                       = MVGMM_EM(xi,K);
    MCR                              = 100*mean(Y~=out_GMM_EM.labels);
    alphas_hat                       = out_GMM_EM.alphas';
    mus_hat                          = out_GMM_EM.mus;
    sigs_hat                         = out_GMM_EM.sigs;
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    sigs_res_all                     = prod(sigs_res_comp);      
    Estimators_GMM_EM(i,:)           = {alphas_hat,mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,sigs_res_all,CR_res.CR_res,MCR};  
    CR_Data_GMM_EM(i,:)              = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};                   
    CR_SigData_GMM_EM(i,:)           = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,CR_res.sig_res_lower,CR_res.sig_res_upper}; 
    
    %%% Estimate with KMeans++ %%%%
    MCR                              = 100*mean(Y~=out_KM.labels);
    alphas_hat                       = out_KM.alphas';
    mus_hat                          = out_KM.mus;
    sigs_hat                         = out_KM.sigs;
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    sigs_res_all                     = prod(sigs_res_comp);                                    
    Estimators_KM(i,:)               = {alphas_hat,mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,sigs_res_all,CR_res.CR_res,MCR};  
    CR_Data_KM(i,:)                  = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};                 
    CR_SigData_KM(i,:)               = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,CR_res.sig_res_lower,CR_res.sig_res_upper};   
    
    %%% Estimate KMeans++ and LL Optimization for sigmas %%%%
    out_KM_LL                        = MVTNM_DC_OptLL(samp,out_KM,r,nu);
    MCR                              = 100*mean(Y~=out_KM_LL.labels);
    alphas_hat                       = out_KM_LL.alphas';
    mus_hat                          = out_KM_LL.mus;
    sigs_hat                         = out_KM_LL.sigs;
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    sigs_res_all                     = prod(sigs_res_comp);                                     
    Estimators_KM_LL(i,:)            = {alphas_hat,mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,sigs_res_all,CR_res.CR_res,MCR};  
    CR_Data_KM_LL(i,:)               = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};         
    CR_SigData_KM_LL(i,:)            = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,CR_res.sig_res_lower,CR_res.sig_res_upper};    
    
    %%% Estimate with TNM EM %%%%
    out_TNM_EM                       = MVTNM_EM(samp,out_KM,K,r,nu);
    MCR                              = 100*mean(Y~=out_TNM_EM.labels);
    alphas_hat                       = out_TNM_EM.alphas';
    mus_hat                          = out_TNM_EM.mus;
    sigs_hat                         = out_TNM_EM.sigs;
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    sigs_res_all                     = prod(sigs_res_comp);                                     
    Estimators_TNM_EM(i,:)           = {alphas_hat,mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,sigs_res_all,CR_res.CR_res,MCR};  
    CR_Data_TNM_EM(i,:)              = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};                  
    CR_SigData_TNM_EM(i,:)           = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,CR_res.sig_res_lower,CR_res.sig_res_upper};
    
    %%% Estimate with TNM EM using radii estimator %%%%
    out_TNM_EM_EstRadii              = MVTNM_EM_EstRadii(samp,out_KM,K,nu,30);
    MCR                              = 100*mean(Y~=out_TNM_EM_EstRadii.labels);
    alphas_hat                       = out_TNM_EM_EstRadii.alphas';
    mus_hat                          = out_TNM_EM_EstRadii.mus;
    sigs_hat                         = out_TNM_EM_EstRadii.sigs;
    r_hat                            = out_TNM_EM_EstRadii.radii;
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    sigs_res_all                     = prod(sigs_res_comp);                             
    Estimators_TNM_EM_EstRadii(i,:)  = {alphas_hat,mus_hat,sigs_hat,r_hat,mus_res_comp,sigs_res_comp,mus_res_all,sigs_res_all,CR_res.CR_res,MCR};  
    CR_Data_TNM_EM_EstRadii(i,:)     = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};                
    CR_SigData_TNM_EM_EstRadii(i,:)  = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,CR_res.sig_res_lower,CR_res.sig_res_upper};
                                    
    parfor_progress; 
end
parfor_progress(0);   

%%%% Organize the output %%%%
out                                  = struct();
out.params                           = params;

% save all the estimator data of the all methods
out.NonEM.Estimators                 = Estimators_nonEM;
out.GMM_EM.Estimators                = Estimators_GMM_EM;
out.KM.Estimators                    = Estimators_KM;
out.MM_LL.Estimators                 = Estimators_KM_LL;
out.TNM_EM.Estimators                = Estimators_TNM_EM;
out.TNM_EM_EstRadii.Estimators       = Estimators_TNM_EM_EstRadii;

% save all the CR data of all the methods
out.NonEM.CR_Data                    = CR_Data_nonEM;
out.GMM_EM.CR_Data                   = CR_Data_GMM_EM;
out.KM.CR_Data                       = CR_Data_KM;
out.MM_LL.CR_Data                    = CR_Data_KM_LL;
out.TNM_EM.CR_Data                   = CR_Data_TNM_EM;
out.TNM_EM_EstRadii.CR_Data          = CR_Data_TNM_EM_EstRadii;

% save all the variance test data of all the methods
out.NonEM.CR_SigData                 = CR_SigData_nonEM;
out.GMM_EM.CR_SigData                = CR_SigData_GMM_EM;
out.KM.CR_SigData                    = CR_SigData_KM;
out.MM_LL.CR_SigData                 = CR_SigData_KM_LL;
out.TNM_EM.CR_SigData                = CR_SigData_TNM_EM;
out.TNM_EM_EstRadii.CR_SigData       = CR_SigData_TNM_EM_EstRadii;

% save summary of estimation results of the different methods
temp  = {alphas',mus,sigs};
TEMP  = {Estimators_nonEM,Estimators_GMM_EM,Estimators_KM,Estimators_KM_LL,Estimators_TNM_EM,Estimators_TNM_EM_EstRadii};
for i=1:6 %number of methods
    TEMP2                            = TEMP{i};
    mean_alphas                      = mean(reshape(cell2mat(TEMP2(:,1)),K,N),2);
    
    sum_mus                          = zeros(K,d);
    sum_sigs                         = zeros(K,d);
    for n=1:N
        sum_mus                      = sum_mus + TEMP2{n,2};
        sum_sigs                     = sum_sigs + TEMP2{n,3};          
    end
    mean_mus                         = 1/N*sum_mus;
    mean_sigs                        = 1/N*sum_sigs;
    temp(i+1,:)                      = {mean_alphas,mean_mus,mean_sigs};
end
out.Est_Stats                          = cell2table(temp);
out.Est_Stats.Properties.VariableNames = {'mean_alphas','mean_mus','mean_sigs'};
out.Est_Stats.Properties.RowNames      = {'True_Values','Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};

% save summary of mean squared errors of the different methods
temp = {};
for i=1:6 %number of methods
    TEMP2                            = TEMP{i};
    alphas_SSE                       = zeros(K,1);
    mus_SSE                          = zeros(K,d);
    sigs_SSE                         = zeros(K,d);
    for n=1:N
        alphas_SSE                   = alphas_SSE + (TEMP2{n,1} - alphas').^2;
        mus_SSE                      = mus_SSE + (TEMP2{n,2} - mus).^2;
        sigs_SSE                     = sigs_SSE + (TEMP2{n,3} - sigs).^2;
    end
    alphas_MSE                       = 1/N*alphas_SSE;
    mus_MSE                          = 1/N*mus_SSE;
    sigs_SSE                         = 1/N*sigs_SSE;
    temp(i,:)                        = {alphas_MSE,mus_MSE,sigs_SSE};
end
out.Est_Err                          = cell2table(temp);
out.Est_Err.Properties.VariableNames = {'alphas_mean_squared_err','mus_mean_squared_err','sigs_mean_squared_err'};
out.Est_Err.Properties.RowNames      = {'Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};

out.MeanMCR                          = table(mean(cell2mat(Estimators_GMM_EM(:,9))),mean(cell2mat(Estimators_KM(:,9))),...
                                        mean(cell2mat(Estimators_KM_LL(:,9))),mean(cell2mat(Estimators_TNM_EM(:,9))),...
                                        mean(cell2mat(Estimators_TNM_EM_EstRadii(:,10))));
out.MeanMCR.Properties.VariableNames = {'GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};

% Save summary of the variance CR results
TEMP = {CR_SigData_nonEM,CR_SigData_GMM_EM,CR_SigData_KM,CR_SigData_KM_LL,CR_SigData_TNM_EM,CR_SigData_TNM_EM_EstRadii};
temp = {};
for i=1:6 %number of methods
    TEMP2                            = TEMP{i};
    mean_LB                          = zeros(K,d);
    mean_UB                          = zeros(K,d);
    lower_prob                       = zeros(K,1);
    upper_prob                       = zeros(K,1);
    for n=1:N
        mean_LB                      = mean_LB + TEMP2{n,1};
        mean_UB                      = mean_UB + TEMP2{n,3};
        lower_prob                   = lower_prob + (TEMP2{n,4})';
        upper_prob                   = upper_prob + (TEMP2{n,5})';
    end
    mean_LB                          = 1/N*mean_LB;
    mean_UB                          = 1/N*mean_UB;
    lower_prob                       = 1/N*lower_prob;
    upper_prob                       = 1/N*upper_prob;
    temp(i,:)                        = {mean_LB,sigs.^2,mean_UB,lower_prob,upper_prob};
end
out.SigStats                          = cell2table(temp);
out.SigStats.Properties.VariableNames = {'mean_LB','sigs_squared','mean_UB','lower_prob','upper_prob'};
out.SigStats.Properties.RowNames      = {'Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};

% save summary of CR results of the different methods 
Z3 = zeros(6,1);
CR_Probs                            = table(Z3,Z3,Z3,Z3);
CR_Probs.Properties.VariableNames   = {'C1_prob','C2_prob','C3_prob','CR_prob'};
CR_Probs.Properties.RowNames        = {'Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};
CR_Probs(1,:)                       = {mean(CR_Data_nonEM.mus_res),mean(CR_Data_nonEM.sigs_res_lower),mean(CR_Data_nonEM.sig_res_upper),mean(CR_Data_nonEM.CR_res)};
CR_Probs(2,:)                       = {mean(CR_Data_GMM_EM.mus_res),mean(CR_Data_GMM_EM.sigs_res_lower),mean(CR_Data_GMM_EM.sig_res_upper),mean(CR_Data_GMM_EM.CR_res)};
CR_Probs(3,:)                       = {mean(CR_Data_KM.mus_res),mean(CR_Data_KM.sigs_res_lower),mean(CR_Data_KM.sig_res_upper),mean(CR_Data_KM.CR_res)};
CR_Probs(4,:)                       = {mean(CR_Data_KM_LL.mus_res),mean(CR_Data_KM_LL.sigs_res_lower),mean(CR_Data_KM_LL.sig_res_upper),mean(CR_Data_KM_LL.CR_res)};
CR_Probs(5,:)                       = {mean(CR_Data_TNM_EM.mus_res),mean(CR_Data_TNM_EM.sigs_res_lower),mean(CR_Data_TNM_EM.sig_res_upper),mean(CR_Data_TNM_EM.CR_res)};
CR_Probs(6,:)                       = {mean(CR_Data_TNM_EM_EstRadii.mus_res),mean(CR_Data_TNM_EM_EstRadii.sigs_res_lower),mean(CR_Data_TNM_EM_EstRadii.sig_res_upper),mean(CR_Data_TNM_EM_EstRadii.CR_res)};
out.CR_Probs                        = CR_Probs;

end