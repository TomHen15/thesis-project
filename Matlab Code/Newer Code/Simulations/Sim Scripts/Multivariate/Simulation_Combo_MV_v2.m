function out = Simulation_Combo_MV_v2(N,M,H,alphas,mus,taus,r,del,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs simulation with mixture of univariate truncated Normals
% This uses estimators for the tuncation radii
% This version solves the optimization problem with
%   - Estimation using the underlying mixture components
%   - Estimation using TNM-EM for the parameter estimation
%   - The one distribution version of DRSP
% Input: 
%   N       = number of simulations
%   M       = sample size
%   H       = function for optimization
%   alphas  = mixture weights
%   mus     = mu parameters of mixture components
%   taus    = tau parameters of mixture components
%   r       = truncation radii of mixture components
%   del     = probability parameter
%   nu      = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[K,d]       = size(mus);    
M1          = Compute_M(alphas,mus,taus,r,del,d);                                   %used in DRSP parameter computation
del_bar     = 1-sqrt(1-del);
%%%% compute true parameter values of truncated normals %%%%
sigs        = Comp_True_Sigmas(mus,taus,r,d);                                       %compute the true standard deviations of the mixture components                                    %compute the true standard deviations of the mixture components
mu_mix      = sum(alphas'.*mus);                                                    %true mixture mean
sig_mix     = zeros(d,d);
for k=1:K
    tmp     = diag(sigs(k,:).^2);
    sig_mix = sig_mix + alphas(k)*(tmp+mus(k,:)'*mus(k,:));
end
sig_mix     = (sqrt(diag(sig_mix - mu_mix'*mu_mix)))';                              %true mixture covariance

%%%% Store all the relevant parameters for output %%%%
params = table(alphas',mus,taus,sigs,r');                                           %save parameters
params.Properties.VariableNames = {'alphas','mus','taus','sigs','truncation_radii'};

%%%% Define output containers %%%%
Z0 = zeros(N,K); Z1 = zeros(N,1);
% output containers for uni-modal DRSP
Estimators_Classic = cell(N,5);                                                     %stores the estimators and the CR results (mus,sigs,mu_res,sig_res,cr-res)
CR_Data_Classic = table(Z1,Z1,Z1,Z1);                                               %stores all the CR test results
CR_Data_Classic.Properties.VariableNames = {'mus_res','sigs_res_lower','sig_res_upper','CR_res'};
OPT_Data_Classic   = table(zeros(N,d),Z1,Z1,Z1,Z1);
OPT_Data_Classic.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_gap','OPT_res'};

% output containers for non_EM version
Estimators_nonEM = cell(N,8);                                                       %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res)
CR_Data_nonEM = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                                        %stores all the CR test results
CR_Data_nonEM.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
OPT_Data_nonEM = table(zeros(N,d),Z1,Z1,Z1,Z1);
OPT_Data_nonEM.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_gap','OPT_res'};

% output containers for TNM-EM version
Estimators_EM = cell(N,9);                                                          %stores the estimators and the CR results (alphas,mus,sigs,mu_res_comp,sig_res_comp,mu_res_all,sig_res_all,cr-res,MCR)
CR_Data_EM = table(Z0,Z0,Z0,Z1,Z1,Z1,Z1);                                           %stores all the CR test results
CR_Data_EM.Properties.VariableNames = {'mus_comp_res','sigs_comp_res_lower','sig_comp_res_upper','mus_res','sigs_res_lower','sig_res_upper','CR_res'};
OPT_Data_EM = table(zeros(N,d),Z1,Z1,Z1,Z1);
OPT_Data_EM.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_gap','OPT_res'};

L = 1000; T = 5; %= floor(M/L);

%%%% perform simulation %%%%
parfor_progress(N);
parfor i = 1:N
    %%%% generate the sample %%%%
    while 1
        samp                         = Sample_MVTNM_DC(M,alphas,mus,taus,r); 
        xi                           = samp.mix;
        Y                            = samp.labels;
        out_KM                       = KmeansPP_DC(xi,K,1);
        MCR                          = 100*mean(Y~=out_KM.labels);
        if MCR < 15
            break
        end
    end
        
    %%%% perform the tests and store results with uni-modal DRSP %%%%
    mus_hat                          = mean(xi);
    sigs_hat                         = (sqrt(diag(cov(xi)))');
    z                                = (samp.mix-mu_mix)./sig_mix;
    R                                = max(sqrt(dot(z,z,2)));
    temp1                            = (R^2+2)^2*(2+sqrt(2*log(4/del_bar)))^2;
    temp2                            = (8+sqrt(32*log(4/del_bar)))^2/(sqrt(R+4)-R)^4;
    M2                               = ceil(max(temp1,temp2));     
    [alpha_bar,beta_bar,gam1,gam2]   = Comp_DRSP_Parameters({xi},mus_hat,sigs_hat,M2,del_bar);
    CR_res                           = Test_CR_MV(mu_mix,sig_mix,mus_hat,sigs_hat,alpha_bar,beta_bar);     
    Estimators_Classic(i,:)          = {mus_hat,sigs_hat,CR_res.mu_res,CR_res.sig_res,CR_res.CR_res};  
    CR_Data_Classic(i,:)             = {CR_res.mu_res,CR_res.sig_res_lower,CR_res.sig_res_upper,CR_res.CR_res};   
    opt_vals                         = zeros(T,1);
    opt_x_vals                       = zeros(T,d);
    mean_h_vals                      = zeros(T,1);
    opt_res_vals                     = zeros(T,1);
    opt_gap_vals                     = zeros(T,1);
    for t=1:T
        %t
        opt                          = DRSPmix_Equivalent_v3(H,{xi((t-1)*L+1:t*L,:)},1,mus_hat',diag(sigs_hat.^2),gam1,gam2);
        opt_vals(t)                  = opt{1};
        opt_x_vals(t,:)              = opt{2};   
        opt_X                        = (repmat(opt{2},M,1)-xi);
        mean_h_vals(t)               = mean(norms(opt_X,2,2));
        opt_res_vals(t)              = mean_h_vals(t) <= opt_vals(t);
        opt_gap_vals(t)              = opt_vals(t) - mean_h_vals(t);
    end
    OPT_Data_Classic(i,:)            = {mean(opt_x_vals),mean(opt_vals),mean(mean_h_vals),mean(opt_gap_vals),mean(opt_res_vals)};
    
    %%%% perform the tests and store results with non_EM version %%%%
    mus_hat                          = cellfun(@mean,samp.comps,'UniformOutput',0);
    mus_hat                          = reshape(cell2mat(mus_hat'),K,d);
    sigs_hat                         = cellfun(@(X) sqrt(diag(cov(X))),samp.comps,'UniformOutput',0);
    sigs_hat                         = reshape(cell2mat(sigs_hat),d,K)';
    [alpha_bar,beta_bar,gam1,gam2]   = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper); 
    Estimators_nonEM(i,:)            = {alphas',mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,CR_res.sig_res,CR_res.CR_res};  
    CR_Data_nonEM(i,:)               = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};      
    etas                             = cell(1,K);
    SIGMAS                           = zeros(d,d,K);
    opt_vals                         = zeros(T,1);
    opt_x_vals                       = zeros(T,d);
    mean_h_vals                      = zeros(T,1);
    opt_res_vals                     = zeros(T,1);
    opt_gap_vals                     = zeros(T,1);
    for t=1:T
        for k=1:K
            temp                     = samp.comps{k};
            etas{k}                  = temp((t-1)*L+1:t*L,:);
            SIGMAS(:,:,k)            = diag(sigs_hat(k,:).^2);
        end      
        opt                          = DRSPmix_Equivalent_v3(H,etas,alphas,mus_hat',SIGMAS,gam1,gam2);
        opt_vals(t)                  = opt{1};
        opt_x_vals(t,:)              = opt{2};   
        opt_X                        = (repmat(opt{2},M,1)-xi);
        mean_h_vals(t)               = mean(norms(opt_X,2,2));
        opt_res_vals(t)              = mean_h_vals(t) <= opt_vals(t);
        opt_gap_vals(t)              = opt_vals(t) - mean_h_vals(t);
    end
    OPT_Data_nonEM(i,:)              = {mean(opt_x_vals),mean(opt_vals),mean(mean_h_vals),mean(opt_gap_vals),mean(opt_res_vals)};
    
    %%%% perform the tests and store results with TNM-EM version %%%%
    MCR                              = 100*mean(Y~=out_KM.labels);
    alphas_hat                       = out_KM.alphas';
    mus_hat                          = out_KM.mus;
    sigs_hat                         = out_KM.sigs;
    [alpha_bar,beta_bar,gam1,gam2]   = Comp_DRSP_Parameters(samp.comps,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;       
    sigs_res_comp_lower              = CR_res.sig_res_lower;   
    sigs_res_comp_upper              = CR_res.sig_res_upper;
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_lower                   = prod(sigs_res_comp_lower);  
    sigs_res_upper                   = prod(sigs_res_comp_upper);                                    
    Estimators_EM(i,:)               = {alphas_hat,mus_hat,sigs_hat,mus_res_comp,sigs_res_comp,mus_res_all,CR_res.sig_res,CR_res.CR_res,MCR};  
    CR_Data_EM(i,:)                  = {mus_res_comp,sigs_res_comp_lower,sigs_res_comp_upper,mus_res_all,sigs_res_lower,sigs_res_upper,CR_res.CR_res};     
    etas                             = cell(1,K);
    SIGMAS                           = zeros(d,d,K);
    opt_vals                         = zeros(T,1);
    opt_x_vals                       = zeros(T,d);
    mean_h_vals                      = zeros(T,1);
    opt_res_vals                     = zeros(T,1);
    opt_gap_vals                     = zeros(T,1);
    for t=1:T
        for k=1:K
            temp                     = xi(out_KM.labels==k,:);
            etas{k}                  = temp((t-1)*L+1:t*L,:);
            SIGMAS(:,:,k)            = diag(sigs_hat(k,:).^2);
        end      
        opt                          = DRSPmix_Equivalent_v3(H,etas,alphas,mus_hat',SIGMAS,gam1,gam2);
        opt_vals(t)                  = opt{1};
        opt_x_vals(t,:)              = opt{2};   
        opt_X                        = (repmat(opt{2},M,1)-xi);
        mean_h_vals(t)               = mean(norms(opt_X,2,2));
        opt_res_vals(t)              = mean_h_vals(t) <= opt_vals(t);
        opt_gap_vals(t)              = opt_vals(t) - mean_h_vals(t);
    end 
    OPT_Data_EM(i,:)                 = {mean(opt_x_vals),mean(opt_vals),mean(mean_h_vals),mean(opt_gap_vals),mean(opt_res_vals)};
    
    parfor_progress;
end
parfor_progress(0);

%%%% Organize the output %%%%
out                                  = struct();
out.params                           = params;

% save all the estimator data of the all methods
out.Classic.Estimators               = Estimators_Classic;
out.nonEM.Estimators                 = Estimators_nonEM;
out.EM.Estimators                    = Estimators_EM;

% save summary of estimation results of the different methods
temp  = {mus,sigs};
TEMP  = {Estimators_nonEM,Estimators_EM};
for i=1:2 %number of methods
    TEMP2                            = TEMP{i};
    sum_mus                          = zeros(K,d);
    sum_sigs                         = zeros(K,d);
    for n=1:N
        sum_mus                      = sum_mus + TEMP2{n,2};
        sum_sigs                     = sum_sigs + TEMP2{n,3};          
    end
    mean_mus                         = 1/N*sum_mus;
    mean_sigs                        = 1/N*sum_sigs;
    temp(i+1,:)                      = {mean_mus,mean_sigs};
end
out.Est_Stats                          = cell2table(temp);
out.Est_Stats.Properties.VariableNames = {'mean_mus','mean_sigs'};
out.Est_Stats.Properties.RowNames      = {'True_Values','Non_EM','EM'};

% save summary of data about CR result for non_EM and EM
Z3 = zeros(3,1);
CR_Probs                            = table(Z3,Z3,Z3,Z3);
CR_Probs.Properties.VariableNames   = {'C1_prob','C2_prob','C3_prob','CR_prob'};
CR_Probs.Properties.RowNames        = {'Classic','Non_EM','EM'};
CR_Probs(1,:)                       = {mean(CR_Data_Classic.mus_res),mean(CR_Data_Classic.sigs_res_lower),mean(CR_Data_Classic.sig_res_upper),mean(CR_Data_Classic.CR_res)};
CR_Probs(2,:)                       = {mean(CR_Data_nonEM.mus_res),mean(CR_Data_nonEM.sigs_res_lower),mean(CR_Data_nonEM.sig_res_upper),mean(CR_Data_nonEM.CR_res)};
CR_Probs(3,:)                       = {mean(CR_Data_EM.mus_res),mean(CR_Data_EM.sigs_res_lower),mean(CR_Data_EM.sig_res_upper),mean(CR_Data_EM.CR_res)};
out.CR_Probs                        = CR_Probs;

% save summary of data about the optimization test in all the methods
Z1 = zeros(3,d); Z2 = zeros(3,1);
OPT_Stats                           = table(Z1,Z2,Z2,Z2,Z2);
OPT_Stats.Properties.VariableNames  = {'mean_opt_x','mean_OptVal','mean_meanh','mean_OptGap','OPT_prob'};
OPT_Stats.Properties.RowNames       = {'Classic','Non_EM','EM'};

OPT_Stats(1,:)                      = {mean(OPT_Data_Classic.OPT_x),mean(OPT_Data_Classic.OPT_val),mean(OPT_Data_Classic.mean_h),mean(OPT_Data_Classic.OPT_gap),mean(OPT_Data_Classic.OPT_res)};
OPT_Stats(2,:)                      = {mean(OPT_Data_nonEM.OPT_x),mean(OPT_Data_nonEM.OPT_val),mean(OPT_Data_nonEM.mean_h),mean(OPT_Data_nonEM.OPT_gap),mean(OPT_Data_nonEM.OPT_res)};
OPT_Stats(3,:)                      = {mean(OPT_Data_EM.OPT_x),mean(OPT_Data_EM.OPT_val),mean(OPT_Data_EM.mean_h),mean(OPT_Data_EM.OPT_gap),mean(OPT_Data_EM.OPT_res)};
out.OPT_Stats                       = OPT_Stats;
                  
% save probability results for all the methods
probs                               = table(CR_Probs.CR_prob,OPT_Stats.OPT_prob);
probs.Properties.VariableNames      = {'CR_Prob','OPT_Prob'};
probs.Properties.RowNames           = {'Classic','Non_EM','EM'};
out.Prob_Stats                      = probs;

% save misc info
out.MeanMCR                          = mean(cell2mat(Estimators_EM(:,9)));

end