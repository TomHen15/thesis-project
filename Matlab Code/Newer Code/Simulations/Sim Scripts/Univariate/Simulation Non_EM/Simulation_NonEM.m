function out = Simulation_NonEM(N,M,H,alphas,mus,taus,r,del)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs simulation with mixture of univariate truncated Normals
% The estimators are computed using the underlying mixture components
% Input:
%   N           = number of simulations
%   M           = sample size
%   H           = optimization target function
%   alphas      = mixture weights
%   mus         = mu parameters of components
%   taus        = tau parameters of components
%   r           = truncation radii of components
%   del         = probability parameter
% Output:
%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,K]      = size(alphas);                                                          %number of mixture components

%%%% compute true parameter values of truncated normals %%%%
sigs  = Comp_True_Sigmas(mus,taus,r,1);                                             %compute the true standard deviations of the mixture components

%%%% Store all the relevant parameters for output %%%%
params = table(alphas,mus,taus,sigs,r,del,M);                                       %save parameters
params.Properties.VariableNames = {'alphas','mus','taus','sigs','truncation_radii','delta','sample_size'};

%%%% define data containers %%%%
Z0 = zeros(N,1); Z1 = zeros(N,K);

Estimators_Data = table(Z1,Z1);                                                     %this stores all the estimators, cluster sizes and MCR
Estimators_Data.Properties.VariableNames = {'mus_hat','sigs_hat'};

CR_Data = table(Z1,Z1,Z1,Z1,Z1,Z1,Z0,Z0,Z0);                                        %this stores all the CR test 
CR_Data.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};

CR_SigData = table(Z1,Z1,Z1,Z1,Z1);                                                 %this stores the data about the variance test
CR_SigData.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

OPT_Data = table(Z0,Z0,Z0,Z0,Z0,Z1,Z1);
OPT_Data.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_res','OPT_gap','gamma1_bar','gamma2_bar'};

%%%% perform simulation %%%%
parfor_progress(N);
parfor i = 1:N 
    %%%% generate the sample %%%%
    samp                    = Sample_TNM(M,alphas,mus,taus,r); 
    
    %%%% perform the tests and store results %%%%
    temp                    = Simulation_NonEM_iter(samp,H,alphas,mus,taus,sigs,r,del);
    Estimators_Data(i,:)    = temp.Estimators;
    CR_Data(i,:)            = temp.CR_Data;
    CR_SigData(i,:)         = temp.CR_SigData;
    OPT_Data(i,:)           = temp.OPT_Data;   
    
    parfor_progress; 
end
parfor_progress(0);

out                         = Simulation_organizeRes(sigs,CR_Data,CR_SigData,OPT_Data);
out.Estimator_Data          = Estimators_Data;
out.params                  = params;

end