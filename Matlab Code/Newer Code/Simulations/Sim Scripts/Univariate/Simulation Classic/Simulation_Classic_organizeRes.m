function out = Simulation_Classic_organizeRes(sig_mix,CR_Data,CR_SigData,OPT_Data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Organizes the output for classic version of simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% compute and store the means of the various estimators:
mean_mu_hat            = mean(CR_Data.mu_hat);
mean_sig_hat           = mean(CR_Data.sig_hat);
C1prob                 = mean(CR_Data.mu_res); 
C2prob                 = mean(CR_SigData.lower_res);
C3prob                 = mean(CR_SigData.upper_res);
CR_Stats               = table(mean_mu_hat,mean_sig_hat,C1prob,C2prob,C3prob,mean(CR_Data.CR_res));
CR_Stats.Properties.VariableNames = {'mean_mu_hat','mean_sig_hat','C1_prob','C2_prob','C3_prob','Combined_prob'};

mean_lb                = mean(CR_SigData.lower_bound);
mean_ub                = mean(CR_SigData.upper_bound);
mean_sig_hat_sq        = mean((CR_Data.sig_hat).^2);
lower_prob             = mean(CR_SigData.lower_res);
upper_prob             = mean(CR_SigData.upper_res);
CR_SigStats            = table(mean_lb,sig_mix.^2,mean_ub,mean_sig_hat_sq,lower_prob,upper_prob);
CR_SigStats.Properties.VariableNames = {'mean_lower_bound','sig_squared','mean_upper_bound','mean_sig_hat_sq','lower_prob','upper_prob'};

%%% store mean statistics about the optimization results 
OPT_Stats = table(mean(OPT_Data.OPT_x),mean(OPT_Data.OPT_val),mean(OPT_Data.mean_h),mean(OPT_Data.OPT_gap),mean(OPT_Data.OPT_res));
OPT_Stats.Properties.VariableNames = {'mean_opt_x','mean_opt_val','mean_mean_h','mean_opt_gap','opt_prob'};

% store all the data and return
out                    = struct();
prob1                  = mean(CR_Data.CR_res);
prob2                  = mean(OPT_Data.OPT_res);
out.probs              = [prob1,prob2];
out.CR_Data            = CR_Data;
out.CR_Stats           = CR_Stats;
out.CR_SigData         = CR_SigData;
out.CR_SigStats        = CR_SigStats;
out.OPT_Data           = OPT_Data;
out.OPT_Stats          = OPT_Stats;

end