function out = Simulation_Classic(N,M,H,alphas,mus,taus,r,del)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs simulation with mixture of univariate truncated Normals
% This uses the uni-modal version of DRSP for the optimization.
% Input:
%   N           = number of simulations
%   M           = sample size
%   H           = optimization target function
%   alphas      = mixture weights
%   mus         = mu parameters for components
%   taus        = tau parameters for components
%   r           = truncation radii for components
%   del         = probability parameter
% Output:
%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
%%%% compute true parameter values of truncated normals %%%%
sigs        = Comp_True_Sigmas(mus,taus,r,1);                                       %compute the true standard deviations of the mixture components
mu_mix      = alphas*mus';                                                          %true mixture mean
sig_mix     = sqrt(alphas*((mus - mu_mix).^2+sigs.^2)');                            %true mixture standard deviation

%%%% Store all the relevant parameters for output
params = table(alphas,mus,taus,sigs,r,del,M);                                       %save parameters
params.Properties.VariableNames = {'alphas','mus','taus','sigs','truncation_radii','delta','sample_size'};

%%%% define data containers %%%%
Z0 = zeros(N,1);
Estimators_Data = table(Z0,Z0);                                                     %this stores all the estimators, cluster sizes and MCR
Estimators_Data.Properties.VariableNames = {'mu_hat','sig_hat'};

CR_Data = table(Z0,Z0,Z0,Z0,Z0,Z0,Z0);                                              %this stores all the CR test 
CR_Data.Properties.VariableNames = {'mu','mu_hat','mu_res','sig','sig_hat','sig_res','CR_res'};

CR_SigData = table(Z0,Z0,Z0,Z0,Z0);                                                 %this stores the data about the variance test
CR_SigData.Properties.VariableNames = {'lower_bound','sig_squared','upper_bound','lower_res','upper_res'};

OPT_Data = table(Z0,Z0,Z0,Z0,Z0,Z0,Z0);
OPT_Data.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_res','OPT_gap','gamma1_bar','gamma2_bar'};

%%%% perform simulation %%%%
parfor_progress(N);
parfor i = 1:N 
    %%%% generate the sample %%%%
    samp                    = Sample_TNM(M,alphas,mus,taus,r); 
    
    %%%%perform the tests and store results %%%%
    temp                    = Simulation_Classic_iter(samp,H,alphas,mus,taus,sigs,r,del);
    Estimators_Data(i,:)    = temp.Estimators;
    CR_Data(i,:)            = temp.CR_Data;
    CR_SigData(i,:)         = temp.CR_SigData;
    OPT_Data(i,:)           = temp.OPT_Data;   
    
    parfor_progress; 
end
parfor_progress(0);

out                    = Simulation_Classic_organizeRes(sig_mix,CR_Data,CR_SigData,OPT_Data);
out.Estimator_Data     = Estimators_Data;
out.params             = params;

end