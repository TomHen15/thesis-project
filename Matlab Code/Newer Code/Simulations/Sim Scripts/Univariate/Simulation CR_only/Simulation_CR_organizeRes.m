function out = Simulation_CR_organizeRes(sigs,CR_Data,CR_SigData)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Organizes the output for NonEM version of simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,K] = size(sigs);

%%% compute and store the means of the various estimators:
mean_mus_hat           = mean(CR_Data.mus_hat);                          
mean_sigs_hat          = mean(CR_Data.sigs_hat);
C1prob                 = mean(CR_Data.mus_res_all); 
C2prob                 = mean(prod(CR_SigData.lower_res,2));
C3prob                 = mean(prod(CR_SigData.upper_res,2));
CR_Stats               = table(mean_mus_hat,mean_sigs_hat,C1prob,C2prob,C3prob,mean(CR_Data.CR_res));
CR_Stats.Properties.VariableNames = {'mean_mus_hat','mean_sigs_hat','C1_prob','C2_prob','C3_prob','Combined_prob'};

mean_lb                = mean(CR_SigData.lower_bounds)';
mean_ub                = mean(CR_SigData.upper_bounds)';
mean_sigs_hat_sq       = mean((CR_Data.sigs_hat).^2)';
lower_prob             = mean(CR_SigData.lower_res)';
upper_prob             = mean(CR_SigData.upper_res)';
CR_SigStats            = table(mean_lb,sigs.^2',mean_ub,mean_sigs_hat_sq,lower_prob,upper_prob);
CR_SigStats.Properties.VariableNames = {'mean_lower_bound','sig_squared','mean_upper_bound','mean_sig_hat_sq','lower_prob','upper_prob'};
names = num2str(1:K)'; names(names==' ')='';
CR_SigStats.Properties.RowNames = cellstr(names);

% store all the data and return
out                    = struct();
out.prob               = mean(CR_Data.CR_res);
out.CR_Data            = CR_Data;
out.CR_Stats           = CR_Stats;
out.CR_SigData         = CR_SigData;
out.CR_SigStats        = CR_SigStats;

end