function out = Simulation_Combo_TrueRadii(N,M,H,alphas,mus,taus,r,del,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs simulation with mixture of univariate truncated Normals
% In this version the real truncation radii are used in the computation
% This version solves the optimization problem with
%   - Estimation using the underlying mixture components
%   - Estimation using TNM-EM for the parameter estimation
%   - The one distribution version of DRSP
%
% Input:
%   N           = number of simulations
%   M           = sample size
%   H           = optimization target function
%   alphas      = mixture weights
%   mus         = mus parameters of components
%   taus        = tau parameters of components
%   r           = truncation radii of components
%   del         = probability parameter
%   nu          = Smoothing parameter for log-likelihood
% Output:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,K]       = size(alphas);                                                         %number of mixture components

%%%% compute true parameter values of truncated normals %%%%
sigs        = Comp_True_Sigmas(mus,taus,r,1);                                       %compute the true standard deviations of the mixture components                                    %compute the true standard deviations of the mixture components
mu_mix      = alphas*mus';                                                          %true mixture mean
sig_mix     = sqrt(alphas*((mus - mu_mix).^2+sigs.^2)');                            %true mixture standard deviation

%%%% Store all the relevant parameters for output
params = table(alphas,mus,taus,sigs,r,del,M);                                       %save parameters
params.Properties.VariableNames = {'alphas','mus','taus','sigs','truncation_radii','delta','sample_size'};

%%%% Define output containers %%%%
Z0 = zeros(N,1); Z1 = zeros(N,K);
% output containers for uni-modal DRSP
Estimators_Data_classic = table(Z0,Z0);                                             %this stores all the estimators, cluster sizes and MCR
Estimators_Data_classic.Properties.VariableNames = {'mu_hat','sig_hat'};

CR_Data_classic = table(Z0,Z0,Z0,Z0,Z0,Z0,Z0);                                      %this stores all the CR test 
CR_Data_classic.Properties.VariableNames = {'mu','mu_hat','mu_res','sig','sig_hat','sig_res','CR_res'};

CR_SigData_classic = table(Z0,Z0,Z0,Z0,Z0);                                         %this stores the data about the variance test
CR_SigData_classic.Properties.VariableNames = {'lower_bound','sig_squared','upper_bound','lower_res','upper_res'};

OPT_Data_classic = table(Z0,Z0,Z0,Z0,Z0,Z0,Z0);
OPT_Data_classic.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_res','OPT_gap','gamma1_bar','gamma2_bar'};

% output containers for non_EM version
Estimators_Data_nonEM = table(Z1,Z1);                                               %this stores all the estimators, cluster sizes and MCR
Estimators_Data_nonEM.Properties.VariableNames = {'mus_hat','sigs_hat'};

CR_Data_nonEM = table(Z1,Z1,Z1,Z1,Z1,Z1,Z0,Z0,Z0);                                  %this stores all the CR test 
CR_Data_nonEM.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};

CR_SigData_nonEM = table(Z1,Z1,Z1,Z1,Z1);                                           %this stores the data about the variance test
CR_SigData_nonEM.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

OPT_Data_nonEM = table(Z0,Z0,Z0,Z0,Z0,Z1,Z1);
OPT_Data_nonEM.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_res','OPT_gap','gamma1_bar','gamma2_bar'};

% output containers for TNM-EM version
Estimators_Data_TNM_EM = table(Z1,Z1,Z1,Z1,Z0);                                         %this stores all the estimators, cluster sizes and MCR
Estimators_Data_TNM_EM.Properties.VariableNames = {'alphas_hat','mus_hat','sigs_hat','cluster_sizes','MCR'};

CR_Data_TNM_EM = table(Z1,Z1,Z1,Z1,Z1,Z1,Z0,Z0,Z0);                                     %this stores all the CR test 
CR_Data_TNM_EM.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};

CR_SigData_TNM_EM = table(Z1,Z1,Z1,Z1,Z1);                                              %this stores the data about the variance test
CR_SigData_TNM_EM.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

OPT_Data_TNM_EM = table(Z0,Z0,Z0,Z0,Z0,Z1,Z1);
OPT_Data_TNM_EM.Properties.VariableNames = {'OPT_x','OPT_val','mean_h','OPT_res','OPT_gap','gamma1_bar','gamma2_bar'};

%%%% perform simulation %%%%
parfor_progress(N);
parfor i = 1:N
    %%%% generate the sample %%%%
    samp                            = Sample_TNM(M,alphas,mus,taus,r);
        
    %%%% perform the tests and store results with uni-modal DRSP %%%%
    temp                            = Simulation_Classic_iter(samp,H,alphas,mus,taus,sigs,r,del);
    Estimators_Data_classic(i,:)    = temp.Estimators;
    CR_Data_classic(i,:)            = temp.CR_Data;
    CR_SigData_classic(i,:)         = temp.CR_SigData;
    OPT_Data_classic(i,:)           = temp.OPT_Data;   

    %%%% perform the tests and store results with non_EM version %%%%
    temp                            = Simulation_NonEM_iter(samp,H,alphas,mus,taus,sigs,r,del);
    Estimators_Data_nonEM(i,:)      = temp.Estimators;
    CR_Data_nonEM(i,:)              = temp.CR_Data;
    CR_SigData_nonEM(i,:)           = temp.CR_SigData;
    OPT_Data_nonEM(i,:)             = temp.OPT_Data;   
    
    %%%% perform the tests and store results with TNM-EM version %%%%
    temp                            = Simulation_TNM_EM_iter(samp,H,alphas,mus,taus,sigs,r,del,nu);
    Estimators_Data_TNM_EM(i,:)     = temp.Estimators;
    CR_Data_TNM_EM(i,:)             = temp.CR_Data;
    CR_SigData_TNM_EM(i,:)          = temp.CR_SigData;
    OPT_Data_TNM_EM(i,:)            = temp.OPT_Data;   
      
    parfor_progress;
end
parfor_progress(0);

%%%% organize the output and return %%%%
Z0 = zeros(2,K); Z1 = zeros(2,1); Z2 = zeros(3,1);
out                                 = struct();                   
out.params                          = params;

% save all the data for each method combined
out.res_classic                     = Simulation_Classic_organizeRes(sig_mix,CR_Data_classic,CR_SigData_classic,OPT_Data_classic);
out.res_nonEM                       = Simulation_organizeRes(sigs,CR_Data_nonEM,CR_SigData_nonEM,OPT_Data_nonEM);
out.res_TNM_EM                      = Simulation_organizeRes(sigs,CR_Data_TNM_EM,CR_SigData_TNM_EM,OPT_Data_TNM_EM);

% save data about the estimators for all the methods
out.res_classic.Estimators_Data     = Estimators_Data_classic;
out.res_nonEM.Estimators_Data       = Estimators_Data_nonEM;
out.res_TNM_EM.Estimators_Data      = Estimators_Data_TNM_EM;

% save summary of data about CR result for non_EM and EM
CR_Stats                            = table(Z0,Z0,Z1,Z1,Z1,Z1);
CR_Stats.Properties.VariableNames   = {'mean_mus_hat','mean_sigs_hat','C1_prob','C2_prob','C3_prob','CR_prob'};
CR_Stats.Properties.RowNames        = {'Non_EM','TNM_EM'};
CR_Stats(1,:)                       = {out.res_nonEM.CR_Stats.mean_mus_hat,out.res_nonEM.CR_Stats.mean_sigs_hat,...
                                      out.res_nonEM.CR_Stats.C1_prob,out.res_nonEM.CR_Stats.C2_prob,...
                                      out.res_nonEM.CR_Stats.C3_prob,out.res_nonEM.CR_Stats.Combined_prob};
CR_Stats(2,:)                       = {out.res_TNM_EM.CR_Stats.mean_mus_hat,out.res_TNM_EM.CR_Stats.mean_sigs_hat,...
                                      out.res_TNM_EM.CR_Stats.C1_prob,out.res_TNM_EM.CR_Stats.C2_prob,...
                                      out.res_TNM_EM.CR_Stats.C3_prob,out.res_TNM_EM.CR_Stats.Combined_prob};
out.CR_Stats                        = CR_Stats;

% save summary of data about the variance part of the CR test
out.Sig_Stats.nonEM                 = out.res_nonEM.CR_SigStats;
out.Sig_Stats.TNM_EM                = out.res_TNM_EM.CR_SigStats;

% save summary of data about the optimization test in all the methods
OPT_Stats                           = table(Z2,Z2,Z2,Z2,Z2);
OPT_Stats.Properties.VariableNames  = {'mean_opt_x','mean_OptVal','mean_meanh','mean_OptGap','OPT_prob'};
OPT_Stats.Properties.RowNames       = {'Classic','Non_EM','TNM_EM'};

OPT_Stats(1,:)                      = {out.res_classic.OPT_Stats.mean_opt_x,out.res_classic.OPT_Stats.mean_opt_val,...
                                       out.res_classic.OPT_Stats.mean_mean_h,out.res_classic.OPT_Stats.mean_opt_gap,out.res_classic.OPT_Stats.opt_prob};
OPT_Stats(2,:)                      = {out.res_nonEM.OPT_Stats.mean_opt_x,out.res_nonEM.OPT_Stats.mean_opt_val,...
                                       out.res_nonEM.OPT_Stats.mean_mean_h,out.res_nonEM.OPT_Stats.mean_opt_gap,out.res_nonEM.OPT_Stats.opt_prob};
OPT_Stats(3,:)                      = {out.res_TNM_EM.OPT_Stats.mean_opt_x,out.res_TNM_EM.OPT_Stats.mean_opt_val,...
                                       out.res_TNM_EM.OPT_Stats.mean_mean_h,out.res_TNM_EM.OPT_Stats.mean_opt_gap,out.res_TNM_EM.OPT_Stats.opt_prob};
out.OPT_Stats                       = OPT_Stats;
                  
% save probability results for all the methods
probs                               = table(out.res_classic.probs',out.res_nonEM.probs',out.res_TNM_EM.probs');
probs.Properties.RowNames           = {'CR_Prob','OPT_Prob'};
probs.Properties.VariableNames      = {'Classic','Non_EM','TNM_EM'};
out.Prob_Stats                      = probs;

% save misc info
out.MeanMCR                          = mean(Estimators_Data_TNM_EM.MCR);

end