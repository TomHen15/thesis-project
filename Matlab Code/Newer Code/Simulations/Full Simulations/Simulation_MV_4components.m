alphas  = [1/4,1/4,1/4,1/4]; taus  = sqrt([1,1;1,1;1,1;1,1]);
mus     = [1.37,1.36;1.35,-1.36;-1.35,1.36;-1.37,-1.36]; [K,d] = size(mus);
r0      = chi2inv(0.60,d); r = repmat(r0,1,K);
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; del_bar = 1-sqrt(1-del);
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 
H       = @(x,y) norm(x-y,2);

ts      = tic;
mus     = [2,2;1.95,-2;-1.95,2;-2,-2];
SIM_MV_4C_0p_Far = Simulation_Combo_MV_v2(200,M,H,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_0p_Far.mat','SIM_MV_4C_0p_Far');
tf      = toc(ts)/60;

mus     = [1.75,1.75;1.73,-1.75;-1.73,1.75;-1.75,-1.75];
SIM_MV_4C_0p_Medium = Simulation_Combo_MV_v2(200,M,H,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_0p_Medium.mat','SIM_MV_4C_0p_Medium');

mus     = [1.35,1.35;1.34,-1.35;-1.34,1.35;-1.35,-1.35];
SIM_MV_4C_0p_near = Simulation_Combo_MV_v2(200,M,H,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_0p_near.mat','SIM_MV_4C_0p_near');

mus     = [1.22,1.19;1.20,-1.19;-1.20,1.19;-1.22,-1.19];
SIM_MV_4C_3p = Simulation_Combo_MV_v2(200,M,H,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_3p.mat','SIM_MV_4C_3p');

mus     = [1.15,1.14;1.13,-1.14;-1.13,1.14;-1.15,-1.14];
SIM_MV_4C_5p = Simulation_Combo_MV_v2(200,M,H,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_5p.mat','SIM_MV_4C_5p');
