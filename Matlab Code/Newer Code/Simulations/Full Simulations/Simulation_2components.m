%% Simulations I want to run
% Two equally weighed clusters with 1-5% misclass - standard sample
% Two equally weighed clusters with 1-5% misclass - double sample 
% Three equally weighed clusters with 1-5 - standard sample
% Three equally weighed clu sters with 1-5 - triple sample
% Same simulations with truncated EM

%%% Simulations with standard Sampling %%%
mus = [4,-4]; % mus should be in decreasing order for sorting purposes.
sigs = [2.2,2.2]; r = [4,4]; alphas = [1/2,1/2];  del = 0.1;
%out = EM_MisclassRate(alphas,mus,sigs,R);
%MCR = out.MCR*100; clear out;
H = @(x,y) abs(x-y); N = 2;

mus = [4,-4]; 
SIM_0_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_0_percent.mat','SIM_0_percent');

mus = [3.8725,-3.8725]; 
SIM_0p5_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_0p5_percent.mat','SIM_0p5_percent');

mus = [3.75,-3.75]; 
SIM_1_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_1_percent.mat','SIM_1_percent');

mus = [3.6475,-3.6475]; 
SIM_1p5_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_1p5_percent.mat','SIM_1p5_percent');

mus = [3.55,-3.55]; 
SIM_2_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_2_percent.mat','SIM_2_percent');

mus = [3.46,-3.46];
SIM_2p5_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_2p5_percent.mat','SIM_2p5_percent');

mus = [3.375,-3.375];
SIM_3_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_3_percent.mat','SIM_3_percent');

mus = [3.29,-3.29];
SIM_3p5_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_3p5_percent.mat','SIM_3p5_percent');

mus = [3.22,-3.22]; 
SIM_4_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_4_percent.mat','SIM_4_percent');

mus = [3.145,-3.145];
SIM_4p5_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_4p5_percent.mat','SIM_4p5_percent');

mus = [3.075,-3.075]; 
SIM_5_percent = Simulation_Combo(N,H,alphas,mus,sigs,r,del);
save('SIM_5_percent.mat','SIM_5_percent');

clear alphas del H M MCR mus N R sigs