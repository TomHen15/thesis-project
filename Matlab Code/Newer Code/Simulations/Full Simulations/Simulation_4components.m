%%% Simulations with standard Sampling - Four Clusters %%%
mus  = [15,7,0,-8]; taus = [2.2,2.2,2.2,2.2]; r = [3.6,3.6,3.6,3.6]; alphas = [1/4,1/4,1/4,1/4]; nu = 100; del = 0.1;
sigs = Comp_True_Sigmas(mus,taus,r,1);
M    = Compute_M_v2(alphas,mus,taus,r,del,1);
H    = @(x,y) abs(x-y); N = 200;

mus = [15,7.5,0,-7.5];
SIM_0_percent_4c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_0_percent_4c.mat','SIM_0_percent_4c');

mus = [13.2,6.75,0,-6.45];
SIM_3_percent_4c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_3_percent_4c.mat','SIM_3_percent_4c');

mus = [12.6,6.7,0,-5.9]; 
SIM_5_percent_4c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_5_percent_4c.mat','SIM_5_percent_4c');