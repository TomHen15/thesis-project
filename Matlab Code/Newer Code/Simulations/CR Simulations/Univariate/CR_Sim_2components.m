mus = [4,-4]; % mus should be in decreasing order for sorting purposes.
taus = [2.2,2.2]; r = [3.8,3.8]; alphas = [1/2,1/2];  del = 0.1; nu = 100;
M = Compute_M_v2(alphas,mus,taus,r,del,1);

mus = [3.8,-3.8]; 
SIM_2C_0_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_0_percent.mat','SIM_2C_0_percent');

mus = [3.605,-3.605]; 
SIM_2C_1_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_1_percent.mat','SIM_2C_1_percent');

mus = [3.41,-3.41]; 
SIM_2C_2_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_2_percent.mat','SIM_2C_2_percent');

mus = [3.25,-3.25];
SIM_2C_3_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_3_percent.mat','SIM_2C_3_percent');

mus = [3.1,-3.1]; 
SIM_2C_4_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_4_percent.mat','SIM_2C_4_percent');

mus = [2.97,-2.97]; 
SIM_2C_5_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_5_percent.mat','SIM_2C_5_percent');

mus = [2.85,-2.85]; 
SIM_2C_6_percent = Simulation_CRtest(500,M,alphas,mus,taus,r,del,nu);
save('SIM_2C_5_percent.mat','SIM_2C_5_percent');

