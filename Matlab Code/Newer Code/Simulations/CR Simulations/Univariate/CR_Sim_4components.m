mus = [15,7,0,-8]; taus = [2.2,2.2,2.2,2.2]; r = [3.6,3.6,3.6,3.6]; alphas = [1/4,1/4,1/4,1/4]; nu = 100; del = 0.1;
sigs = Comp_True_Sigmas(mus,taus,r,1);
M = Compute_M_v2(alphas,mus,taus,r,del,1);

T = TNM_EM_MisclassRate(M,alphas,[12.6,6.7,0,-5.9],taus,r,nu);
T.MCR 
dist([12.95,6.7,0,-6.25])

mus = [15,7.5,0,-7.5];
SIM_4C_0_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_0_percent.mat','SIM_4C_0_percent');

mus = [13.95,7,0,-6.95]; 
SIM_4C_1_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_1_percent.mat','SIM_4C_1_percent');

mus = [13.5,6.75,0,-6.75]; 
SIM_4C_2_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_2_percent.mat','SIM_4C_2_percent');

mus = [13.2,6.75,0,-6.45];
SIM_4C_3_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_3_percent.mat','SIM_4C_3_percent');

mus = [12.9,6.7,0,-6.2]; 
SIM_4C_4_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_4_percent.mat','SIM_4C_4_percent');

mus = [12.6,6.7,0,-5.9]; 
SIM_4C_5_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_5_percent.mat','SIM_4C_5_percent');

mus = [12.4,6.7,0,-5.7]; 
SIM_4C_6_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_4C_6_percent.mat','SIM_4C_5_percent');

