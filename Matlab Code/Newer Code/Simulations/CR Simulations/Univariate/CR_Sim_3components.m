mus = [7.3,0,-7.3]; taus = [2.2,2.2,2.2]; r = [3.6,3.6,3.6]; alphas = [1/3,1/3,1/3]; nu = 100; del = 0.1;
sigs = Comp_True_Sigmas(mus,taus,r,1);
M = Compute_M_v2(alphas,mus,taus,r,del,1);

mus = [7.3,0,-7.3]; 
SIM_3C_0_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_3C_0_percent.mat','SIM_3C_0_percent');

mus = [6.925,0,-6.925]; 
SIM_3C_1_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_3C_1_percent.mat','SIM_3C_1_percent');

mus = [6.68,0,-6.68]; 
SIM_3C_2_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_3C_2_percent.mat','SIM_3C_2_percent');

mus = [6.45,0,-6.45];
SIM_3C_3_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_3C_3_percent.mat','SIM_3C_3_percent');a

mus = [6.238,0,-6.238]; 
SIM_3C_4_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_3C_4_percent.mat','SIM_3C_4_percent');

mus = [6.05,0,-6.05]; 
SIM_3C_5_percent = Simulation_CRtest_v2(200,M,alphas,mus,taus,r,del,nu);
save('SIM_3C_5_percent.mat','SIM_3C_5_percent');


