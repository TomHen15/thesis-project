function out = DRSPmix_Equivalent(H,etas,alphas,MUS,SIGMAS,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % etas      = cell array of mixture component data
    % alphas    = vector of mixture coefficients
    % MUS       = matrix of mean vectors for components
    % SIGMAS    = 3d array of covariance matrices for components
    % gam1      = vector of gamma1 constants
    % gam2      = vector of gamma2 constants
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This version uses nested loops for the r constraint
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((min(gam1)>=0)&&(min(gam2)>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    
    [n,~] = size(MUS);      % dimension of vectors in problem
    [~,K] = size(gam1);     % number of mixture components
    [N,~] = size(etas{1});  % number of sampled vectors
    
    H2 = @(x,Q) x*Q*x';
    H3 = @(x,y) x*y;
    
    cvx_begin quiet
        variables x(1,n) q(n,K) r(K) t(K) Q(n,n,K)
        expressions T1(K) T2(K) T3(K) 
        for k=1:K
            T1(k) = trace((gam2(k)*SIGMAS(:,:,k) + MUS(:,k)*MUS(:,k)') * Q(:,:,k));
            T2(k) = norm(sqrtm(SIGMAS(:,:,k)) * (q(:,k) + 2*Q(:,:,k)*MUS(:,k)),2);
            T3(k) = sqrt(gam1(k))*T2(k) + MUS(:,k)'*q(:,k);
        end
        minimize(sum(r) + sum(t))
        subject to
            for k = 1:K
                curr_eta = etas{k};
                for j=1:N
                    r(k) >= alphas(k)*H(x,curr_eta(j,:)) -...
                        quad_form(curr_eta(j,:),Q(:,:,k)) - q(:,k)'*curr_eta(j,:)';
                end 
                t(k) >= T1(k) + T3(k);
                Q(:,:,k) == semidefinite(n);
            end   
    cvx_end
    out = {cvx_optval,x};
end