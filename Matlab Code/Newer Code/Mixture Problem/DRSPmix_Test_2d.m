%%%% This is for test of equivalence between different mDRSP solvers in two-dimensions %%%%
n = 2;                              %dimension of problem
K = 3;                              %number of mixture components
N = 10000;                          %number of xi vectors
alphas = [1/3,1/3,1/3];
MUS = [1.7,1.8;0,-0.5;-1.7,1.8]';
SIGMAS = zeros(n,n,K);
for k=1:K
    SIGMAS(:,:,k) = eye(n);
end
% generate the mixture components
etas = cell(K,1);
for k=1:K
    etas{k} = mvnrnd(MUS(:,k),SIGMAS(:,:,k),N);
end
gam1 = [1,1,1]; gam2 = [1,1,1];

%%
H = @(x,y) norm(x-y,2);

ts     = tic;
out1   = DRSPmix_Equivalent(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
ttot_1 = toc(ts)/60;

ts     = tic;
out2   = DRSPmix_Equivalent_v2(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
ttot_2 = toc(ts)/60;

out1 = [out1{1},out1{2}]
out2 = [out2{1},out2{2}]

clear MUS alphas SIGMAS etas H mix A alpha k n N K z ts