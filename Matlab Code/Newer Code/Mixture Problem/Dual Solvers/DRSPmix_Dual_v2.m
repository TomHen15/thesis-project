function out = DRSPmix_Dual_v2(H,etas,alphas,MUS,SIGMAS,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % xis       = sampled xi vectors
    % alphas    = vector of mixture coefficients
    % MUS       = matrix of mean vectors for components
    % SIGMAS    = 3D array of covariance matrices for components
    % gam1      = vector of gamma1 constants 
    % gam2      = vector of gamma2 constants
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This version uses cellfun for the r constraint
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((min(gam1)>=0)&&(min(gam2)>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    
    [n,~] = size(MUS);      % dimension of vectors in problem
    [~,K] = size(gam1);     % number of mixture components
    [N,~] = size(etas{1});  % number of sampled vectors
    
    H2 = @(x,Q) x*Q*x';
    H3 = @(x,y) x*y;
    
    cvx_begin quiet
        variables x(1,n) p(K,n) r(K) s(K) Q(K,n,n) P(K,n,n)  
        expressions T1(K) T2(K) T3(K) T4(K) T5(K,n) Z(K,n+1,n+1) W(K,N)
        for k = 1:K 
            T1(k) = trace((gam2(k)*SIGMAS(:,:,k) - MUS(:,k)*MUS(:,k)') * squeeze(Q(k,:,:)));
            T2(k) = trace(SIGMAS(:,:,k) * squeeze(P(k,:,:)));
            T3(k) = -2*MUS(:,k)'*p(k,:)';
            T4(k) = gam1(k)*s(k);
            T5(k,:) = 2*(p(k,:)' + squeeze(Q(k,:,:))*MUS(:,k));
            Z(k,:,:) = [squeeze(P(k,:,:)),p(k,:)';p(k,:),s(k)];
            %%%%
            H1 = @(x,y) alphas(k)*H(x,y);
            Z1 = mat2cell(etas{k},ones(N,1),n);
            Z2 = cell(N,1); Z2(:) = {x};
            Z3 = cell(N,1); Z3(:) = {squeeze(Q(k,:,:))};
            Z4 = cell(N,1); Z4(:) = {T5(k,:)'};
            ZZ1 = cellfun(H1,Z1,Z2,'UniformOutput',0);
            ZZ2 = cellfun(H2,Z1,Z3,'UniformOutput',0);
            ZZ3 = cellfun(H3,Z1,Z4,'UniformOutput',0);
            ZZZ1 = cellfun(@minus,ZZ1,ZZ2,'UniformOutput',0);
            ZZZ2 = cellfun(@plus,ZZZ1,ZZ3,'UniformOutput',0);
            for j=1:N
                W(k,j) = ZZZ2{j};
            end
        end
        minimize(sum(r+T1+T2+T3+T4))
        subject to
            for k = 1:K
                %curr_eta = etas{k};
                %for j=1:N
                %    r(k) >= alphas(k)*H(x,curr_eta(j,:)) -...
                %        quad_form(curr_eta(j,:),squeeze(Q(k,:,:))) + T5(k,:)*curr_eta(j,:)';
                %end 
                r(k) >= W;
                squeeze(Q(k,:,:)) == semidefinite(n);
                squeeze(Z(k,:,:)) == semidefinite(n+1);
            end 
    cvx_end
    out = {cvx_optval,x};
end