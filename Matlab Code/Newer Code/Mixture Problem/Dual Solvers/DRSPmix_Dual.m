function out = DRSPmix_Dual(H,etas,alphas,MUS,SIGMAS,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % xis       = sampled xi vectors
    % alphas    = vector of mixture coefficients
    % MUS       = matrix of mean vectors for components
    % SIGMAS    = 3D array of covariance matrices for components
    % gam1      = vector of gamma1 constants 
    % gam2      = vector of gamma2 constants
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This version uses nested loops for the r constraint
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((min(gam1)>=0)&&(min(gam2)>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    
    [n,~] = size(MUS);      % dimension of vectors in problem
    [~,K] = size(gam1);     % number of mixture components
    [N,~] = size(etas{1});  % number of sampled vectors
    
    H2 = @(x,Q) x*Q*x';
    H3 = @(x,y) x*y;
    
    cvx_begin quiet
        variables x(1,n) p(n,K) r(K) s(K) Q(n,n,K) P(n,n,K)  
        expressions T1(K) T2(K) T3(K) T4(K) T5(K,n) Z(n+1,n+1,K) %W(K,N)
        for k = 1:K 
            T1(k) = trace((gam2(k)*SIGMAS(:,:,k) - MUS(:,k)*MUS(:,k)') * Q(:,:,k));
            T2(k) = trace(SIGMAS(:,:,k) * P(:,:,k));
            T3(k) = -2*MUS(:,k)' * p(:,k);
            T4(k) = gam1(k) * s(k);
            T5(k,:) = 2*(p(:,k) + Q(:,:,k)*MUS(:,k));
            Z(:,:,k) = [P(:,:,k),p(:,k);p(:,k)',s(k)];
        end
        minimize(sum(r+T1+T2+T3+T4))
        subject to
            for k = 1:K
                curr_eta = etas{k};
                for j=1:N
                    r(k) >= alphas(k)*H(x,curr_eta(j,:)) -...
                        quad_form(curr_eta(j,:),Q(:,:,k)) + T5(k,:)*curr_eta(j,:)';
                end 
                Q(:,:,k) == semidefinite(n);
                Z(:,:,k) == semidefinite(n+1);
            end 
    cvx_end
    out = {cvx_optval,x};
end