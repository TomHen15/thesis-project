function out = DRPOmix_Equivalent(alphas,MUS,SIGMAS,gam1,gam2)
    %% Input:
    % alphas    = vector of mixture coefficients
    % MUS       = matrix of mean vectors for components
    % SIGMAS    = 3d array of covariance matrices for components
    % gam1      = vector of gamma1 constants
    % gam2      = vector of gamma2 constants
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((min(gam1)>=0)&&(min(gam2)>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    
    [n,~] = size(MUS); % dimension of vectors in problem
    M     = size(gam1);   % number of mixture components
    
    cvx_begin quiet
        variables x(n) p(n,M) q(n,M) r(M) s(M)
        variable Q(n,n,M) symmetric    
        variable P(n,n,M) symmetric
        expressions T1(M) T2(M) T3(M) T4(M) Z1(n+1,n+1,M) Z2(n+1,n+1,M) 
        for j = 1:M
            T1(j) = trace((gam2(j)*SIGMAS(:,:,j) - MUS(:,j)*MUS(:,j)') * Q(:,:,j));
            T2(j) = trace(SIGMAS(:,:,j) * P(:,:,j));
            T3(j) = -2*dot(MUS(:,j),p(:,j));
            T4(j) = gam1(j)*s(j);
            Z1(:,:,j) = [P(:,:,j),p(:,j);p(:,j)',s(j)];
            Z2(:,:,j) = [Q(:,:,j),(alphas(j)*x+q(:,j))/2;(alphas(j)*x+q(:,j))'/2,r];
        end
        minimize(sum(r+T1+T2+T3+T4))
        subject to
            for j = 1:M
                p(:,j) == -q(:,j)/2 - Q(:,:,j)*MUS(:,j);
                Z1(:,:,j) == semidefinite(n+1);
                Z2(:,:,j) == semidefinite(n+1);
            end 
            sum(x) == 1;
            x >= zeros(n,1);
    cvx_end
    out = {x,cvx_optval,Q,P,q,p,r,s};
end