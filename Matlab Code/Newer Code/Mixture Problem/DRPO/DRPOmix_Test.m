%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% generate data
n = 2;                              %dimension of problem
M = 2;                              %number of mixture components
N = 10000;                            %number of xi vectors
alpha  = 0.3;                       %mixture coefficient
alphas = [alpha,1-alpha];                 

%create mean vectors for components
MUS = randn(n,M);                 
%create covariances for distributional family
SIGMAS = zeros(n,n,M);
for j=1:M
    A = randn(n,n);
    %SIGMAS(:,:,j) = A'*A;    
    SIGMAS(:,:,j) = eye(n);
end 

% generate the mixture components
xis = zeros(N,n,M);
for j=1:M
    xis(:,:,j) = mvnrnd(MUS(:,j),SIGMAS(:,:,j),N);
end
% generate the mixing vector
z = binornd(1,alpha,N,1);
mix = z.*xis(:,:,1) + (1-z).*xis(:,:,2);  
mix = mix';

%% set parameters of distribution family

out1 = DRPOmix_Equivalent(alphas,MUS,SIGMAS,1,1); 
out2 = DRPOmix_Dual(mix,alphas,MUS,SIGMAS,1,1); 
