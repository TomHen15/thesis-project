function out = DRSP_Equivalent(H,xis,mu0,sigma0,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % xis       = sampled xi vectors
    % mu0       = mean vector for distributional family
    % sigma0    = covariance matrix for distributional family
    % gam1,gam2 = constants for distributional family
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((gam1>=0)&&(gam2>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    [n,~] = size(mu0);
    [~,N] = size(xis);
    cvx_begin quiet
        variables x(n) q(n) r t 
        variable Q(n,n) symmetric
        minimize(r + t)
        subject to
            for k = 1:N
                r >= H(x,xis(:,k)) - quad_form(xis(:,k),Q) - dot(q,xis(:,k));
            end
            expressions T1 T2 
            T1 = trace((gam2 * sigma0 + mu0 * mu0')' * Q);
            T2 = sqrtm(sigma0);
            t >= T1 + dot(mu0,q) + sqrt(gam1) * norm(T2 * (q + 2 * Q * mu0),2);
            Q == semidefinite(n);
    cvx_end
    out = {cvx_optval,x,Q,q,r,t};
end