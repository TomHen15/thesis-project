%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% generate data
n = 2;                              %dimension of problem
N = 100;                            %number of xi vectors
mu0 = randn(n,1);                   %mean for distributional family
A = randn(n,n);                     
sig0 = A'*A;                        %covariance for distributional family
xis = mvnrnd(mu0,sig0,N);   %draw IID random normal xis with mean 1 and variance 1
xis = xis';

%% set parameters of D(R^n,mu0,sigma0,gamma1,gamma2) and solve for target H
H = @(x,y) norm(x-y);                      %optimization target
%H = @(x,y) x'*y;

out1 = DRSP_Equivalent(H,xis,mu0,sig0,1,1);  %this solves using the equivalent problem
out2 = DRSP_Dual(H,xis,mu0,sig0,1,1);  %this solves using the dual problem

%% Test the mixture script with only one component

out1 = DRSP_Equivalent(H,xis,mu0,sig0,1,1);  
out2 = DRSPmix_Equivalent(H,xis,1,mu0,sig0,1,1); 
out3 = DRSPmix_Dual(H,xis,1,mu0,sig0,1,1); 
