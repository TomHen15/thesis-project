function out = Dual_standard(H,xis,mu0,sigma0,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % xis       = sampled xi vectors
    % mu0       = mean vector for distributional family
    % sigma0    = covariance matrix for distributional family
    % gam1,gam2 = constants for distributional family
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((gam1>=0)&&(gam2>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    [n,p] = size(mu0);
    [p,N] = size(xis);
    cvx_begin quiet
        variables x(n) q(n) p(n) r s
        variable Q(n,n) symmetric
        variable P(n,n) symmetric
        expressions T1 T2 Z
        T1 = trace((gam2 * sigma0 - mu0 * mu0')' * Q);
        T2 = trace(sigma0 * P);
        Z = [P,p;p',s];
        minimize(T1 + T2  - 2*mu0'*p + gam1*s +r)
        subject to
            for k = 1:N
                r >= H(x,xis(:,k)) - quad_form(xis(:,k),Q) + 2*dot((p + Q*mu0),xis(:,k));
            end
            Q == semidefinite(n);
            Z == semidefinite(n+1);
    cvx_end
    out = {x,cvx_optval,Q,P,p,r,s};
end