function out = OneDimTest3(xis,mu0,sig0,gam1,gam2)
    C = sqrt(gam1)*sqrt(sig0);
    D = gam2*sig0+mu0^2;
    N = length(xis);
    A = [];
    w = [];
    for i=1:N
        A = [A;[1,-xis(i)^2,-xis(i),-1,0];[-1,-xis(i)^2,-xis(i),-1,0]];
        w = [w;xis(i);-xis(i)];
    end
    A2 = [0,(D+2*C*mu0),mu0+C,0,-1;0,(D-2*C*mu0),mu0-C,0,-1;0,-1,0,0,0];
    A = [A;A2];
    w = [w;0;0;0];
    f = [0,0,0,1,1];
    options = optimoptions('linprog','Algorithm','dual-simplex');
    [v,fval] = linprog(f,A,w,[],[],[],[],[],options);
    out = [fval,v'];
end 