function out = OneDimTest1(H,xis,mu0,sig0,gam1,gam2)
    N = length(xis);
    cvx_begin quiet
        variables x r t q Q
        minimize(r+t)
        subject to
            for k = 1:N
                r >= H(x,xis(k)) - q*xis(k) - xis(k)^2*Q;
            end
            t >= (gam2*sig0+mu0^2)*Q + mu0*q +sqrt(gam1)*abs(sqrt(sig0)*(q+2*Q*mu0));
            Q >= 0;
    cvx_end
    out = {cvx_optval,x,Q,q,r,t,};
end 