function out = OneDimTest2xy(xis,mu0,sig0,gam1,gam2)
    C = sqrt(gam1)*sqrt(sig0);
    D = gam2*sig0+mu0^2;
    N = length(xis);
    cvx_begin quiet
        variables x r t q Q
        minimize(r+t)
        subject to
            for i=1:N
                x*xis(i) - xis(i)^2*Q - xis(i)*q -r <= 0
            end
            (D + 2*C*mu0)*Q + (mu0 + C)*q -t <= 0
            (D - 2*C*mu0)*Q + (mu0 - C)*q -t <= 0
            -Q <= 0
    cvx_end
    out = {x,Q,q,r,t,cvx_optval};
end 