%% Test one dimensional case for h(x,y) = |x-y|
mu0 = rand(1);
sig0 = rand(1);
gam1 = 3-3*rand(1);
gam2 = 3-2*rand(1);

N = 8000;
xis = randn(1,N);         %use normal xis (non-compact support)
xis = unifrnd(-3,3,1,N);  %use uniform xis (compact support)
H = @(x,y) abs(x-y);

OneDimTest1(H,xis,mu0,sig0,gam1,gam2) %This tests using "naive" form of absolute value constraints
DRSP_Equivalent(H,xis,mu0,sig0,gam1,gam2)      %This tests using the general "high-dimensional" code
OneDimTest2(xis,mu0,sig0,gam1,gam2)   %This tests using "converted" form of absolute value constraints
OneDimTest3(xis,mu0,sig0,gam1,gam2)   %This tests using Linprog to solve the converted linear constraints

%% Test one dimensional case for h(x,y) = x*y
mu0 = rand(1);
sig0 = rand(1);
gam1 = 3-3*rand(1);
gam2 = 3-2*rand(1);

N = 1000;
xis = randn(1,N);         %use normal xis (non-compact support)
xis = unifrnd(-3,3,1,N);  %use uniform xis (compact support)

mu0 = 1;
sig0 = 1;
gam1 = 1;
gam2 = 1;
xis = [-1,1];

H = @(x,y) x*y;

OneDimTest1(H,xis,mu0,sig0,gam1,gam2)   %This tests using "naive" form of absolute value constraints
DRSP(H,xis,mu0,sig0,gam1,gam2)        %This tests using the general "high-dimensional" code
OneDimTest2xy(xis,mu0,sig0,gam1,gam2)   %This tests using "converted" form of absolute value constraints
OneDimTest3xy(xis,mu0,sig0,gam1,gam2)   %This tests using Linprog to solve the converted linear constraints
