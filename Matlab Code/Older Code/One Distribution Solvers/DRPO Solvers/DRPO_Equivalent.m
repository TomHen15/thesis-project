function out = DRPO_Equivalent(mu0,sigma0,gam1,gam2)
    %% Input:
    % mu0       = mean vector for distributional family
    % sigma0    = covariance matrix for distributional family
    % gam1,gam2 = constants for distributional family
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((gam1>=0)&&(gam2>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    [n,~] = size(mu0);
    
    cvx_begin quiet
        variables x(n) p(n) q(n) r s
        variable Q(n,n) symmetric
        variable P(n,n) symmetric
        expressions Z1 Z2 
        Z1 = [P,p;p',s];
        Z2 = [Q,(x+q)/2;(x+q)'/2,r];
        minimize(gam2*trace(sigma0*Q) - quad_form(mu0,Q) + trace(sigma0*P) - 2*dot(mu0,p) + gam1*s + r)
        subject to
            sum(x) == 1;
            x >= zeros(n,1);
            p == -q/2 - Q*mu0;
            Z1 == semidefinite(n+1);
            Z2 == semidefinite(n+1);
    cvx_end
    out = {x,cvx_optval,Q,P,q,p,r,s};
end