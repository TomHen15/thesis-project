function out = DRPO_Dual(xis,mu0,sigma0,gam1,gam2)
    %% Input:
    % xis       = sampled xi vectors
    % mu0       = mean vector for distributional family
    % sigma0    = covariance matrix for distributional family
    % gam1,gam2 = constants for distributional family
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((gam1>=0)&&(gam2>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end
    [n,~] = size(mu0);
    [~,N] = size(xis);
    cvx_begin quiet
        variables x(n) p(n) q(n) r s
        variable Q(n,n) symmetric
        variable P(n,n) symmetric
        expression Z  
        Z = [P,p;p',s];
        minimize(gam2*trace(sigma0*Q) - quad_form(mu0,Q) + trace(sigma0*P) - 2*dot(mu0,p) + gam1*s + r)
        subject to
            for k = 1:N
                quad_form(xis(:,k),Q) + dot(q,xis(:,k)) + dot(x,xis(:,k)) + r >=0
            end
            sum(x) == 1;
            x >= zeros(n,1);
            p == -q/2 - Q*mu0;
            Q == semidefinite(n);
            Z == semidefinite(n+1);
    cvx_end
    out = {x,cvx_optval,Q,P,q,p,r,s};
end