%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% generate data
n = 2;                              %dimension of problem
N = 100;                            %number of xi vectors
mu0 = randn(n,1);                   %mean for distributional family
A = randn(n,n);                     
sig0 = A'*A;                        %covariance for distributional family
xis = mvnrnd(mu0,sig0,N);   %draw IID random normal xis with mean 1 and variance 1
xis = xis';                  %covariance for distributional family

%% Test 

out1 = DRPO_Equivalent(mu0,sig0,1,1); 
out2 = DRPO_Dual(xis,mu0,sig0,1,1); 

%% Test the mixture script with only one component

out1 = DRPO_Equivalent(mu0,sig0,1,1);
out2 = DRPO_Dual(xis,mu0,sig0,1,1); 
out3 = DRPOmix_Equivalent(1,mu0,sig0,1,1); 
out4 = DRPOmix_Dual(xis,1,mu0,sig0,1,1); 