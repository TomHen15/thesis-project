%% Example of sampling from MV-TN
mu           = [2,2]; [~,d] = size(mu);
tau          = diag([3,5]);
r            = chi2inv(0.8,d); 
M1           = Compute_M_v2(1,mu,tau,r,0.1,d);
xi1          = mvnrnd(mu,tau,M1);        %untruncated sample
xi2          = Sample_MVTN(M1,mu,tau,r); %truncated sample

gridx1       = min(xi1(:,1)):.1:max(xi1(:,1));
gridx2       = min(xi1(:,2)):.1:max(xi1(:,2));
[x1,x2]      = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];

figure(1)
ksdensity(xi1,x); % untruncated

figure(2)
ksdensity(xi2,x); % truncated 

%% Example of sampling MV-TNM with K = 2
alphas      = [0.5,0.5]; 
mus         = [5,5;1,1]; [~,d] = size(mus);
taus        = sqrt([1,1;2,3]);
r0          = chi2inv(0.7,d); r = [r0,r0];
M1          = Compute_M_v2(alphas,mus,taus,r,0.1,d);
samp        = Sample_MVTNM_DC(M1,alphas,mus,taus,r);
xi1         = samp.mix;

gridx1      = min(xi1(:,1)):.1:max(xi1(:,1));
gridx2      = min(xi1(:,2)):.1:max(xi1(:,2));
[x1,x2]     = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];

figure(1)
ksdensity(xi1,x);

%% Example of variance computation using numerical integration
mu          = [2,3]; tau = [3,5]; [~,d] = size(mu);
r           = chi2inv(0.85,d); 
xi          = Sample_MVTN(1000000,mu,diag(tau),r);
sig_est     = sqrt(diag(cov(xi)));
sig_comp    = Comp_MVTN_Variance(mu,sqrt(tau),r);
sig_comp2   = Comp_True_Sigmas(mu,sqrt(tau),r,2);

%% Example of sample size computation  
% univariate calculation
alphas      = [0.5,0.5]; mus = [5,5]; taus = [2,2]; alpha = 0.95;
r_1dim      = taus.*sqrt(chi2inv(alpha,1)); 
sigs        = Comp_True_Sigmas(mus,taus,r_1dim,1);
R_1dim      = r_1dim./sigs;
M1          = Compute_M(alphas,mus,taus,r_1dim,0.1,1);

% multivariate calculation
alphas      = [0.5,0.5]; 
mus         = [5,5;-2,-2]; [~,d] = size(mus);
taus        = sqrt([2,3;2,3]);
r0 = chi2inv(alpha,d); r_ddim = [r0,r0];
M2          = Compute_M(alphas,mus,taus,r_ddim,0.1,d);

%% Example of sample size behavior in r
del = 0.1; del_bar = 1-sqrt(1-del);
fun1 = @(r) (r.^2+2).^2.*(2+sqrt(2*log(4/del_bar))).^2;
fun2 = @(r) ((8+sqrt(32*log(4/del_bar))).^2)./((sqrt(r+4)-r).^4);
rvals = (linspace(1,10,100000))';
fvals1 = fun1(rvals);
fvals2 = fun2(rvals);
figure(1)
plot(rvals,log(fvals1))
hold on
plot(rvals,log(fvals2))
z = [rvals,fvals1,fvals2];

figure(2)
fun = @(r) ceil(max((r.^2+2).^2.*(2+sqrt(2*log(4/del_bar))).^2,...
            (8+sqrt(32*log(4/del_bar))).^2./(sqrt(r+4)-r).^4));
rvals = (linspace(1,10,100000))';
fvals = fun(rvals);
plot(rvals,log(fvals))
title('Log Sample Size as function of $\hat{R}$','Interpreter','latex','FontSize',18)
z = [rvals,fvals];

%% Example of DRSP parameter computation 
% univariate calculation
alphas      = [0.5,0.5]; mus = [3.5,-3.5]; taus = [2,2]; 
r_1dim      = taus.*sqrt(chi2inv(0.8,1)); 
sigs        = Comp_True_Sigmas(mus,taus,r_1dim,1);
R_1dim      = r_1dim./sigs;
M1          = Compute_M(alphas,mus,taus,r_1dim,0.1,1);
samp        = Sample_TNM(M1,alphas,mus,taus,r_1dim);
%histogram(samp.mix)
etas        = num2cell(samp.comps,1);
del_bar     = 1-sqrt(1-0.1);
[alpha_bar1,beta_bar1,gamma1_bar1,gamma2_bar1] = Comp_DRSP_Parameters(etas,mus,sigs,M1,del_bar);

% multivariate calculation
alphas      = [0.5,0.5]; 
mus         = [5,5;2,2]; [~,d] = size(mus);
taus        = sqrt([1,1;2,3]);
r0          = chi2inv(0.7,d); r_ddim = [r0,r0];
sigs        = Comp_True_Sigmas(mus,taus,r_ddim,2);
M2          = Compute_M(alphas,mus,taus,r_ddim,0.1,d);
samp        = Sample_MVTNM_DC(M2,alphas,mus,taus,r_ddim);
etas        = samp.comps;
del_bar     = 1-sqrt(1-0.1);

for k=1:2
    r    = r_ddim;
    fun  = @(z) 1/sqrt(2*pi)/chi2cdf(r(k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r(k)-z.^2,d-1);
    C    = integral(fun,-sqrt(r(k)),sqrt(r(k)));
    R_ddim(k) = sqrt(r(k)/C);
end 

[alpha_bar2,beta_bar2,gamma1_bar2,gamma2_bar2] = Comp_DRSP_Parameters(etas,mus,sigs,M2,del_bar);

clear alphas mus taus C d del_bar etas fun k r r0 samp sigs taus

%% Example of testing Kmeans on MVTNM data
alphas       = [0.5,0.5]; 
mus          = [5,5;2,2]; [~,d] = size(mus);
taus         = sqrt([1,1;2,3]);
r0           = chi2inv(0.8,d); r = [r0,r0];
nu           = 15; del = 0.1;
M1           = Compute_M_v2(alphas,mus,taus,r,del,d); 
samp         = Sample_MVTNM_DC(M1,alphas,mus,taus,r);
xi           = samp.mix;

out_KM         = KmeansPP_DC(xi,2,1);
mean(out_KM.labels~=samp.labels)
alphas_hat   = out_KM.alphas;
mus_hat      = out_KM.mus;
taus_hat     = out_KM.sigs;

Y_hat = MVTNM_DC_LLClassify(xi,alphas_hat,mus_hat,taus_hat,r,nu);
mean(Y_hat~=samp.labels)

%% Example of estimation of taus with LL optimization
alphas      = [0.5,0.5];
mus         = [5,5;1,1];
taus        = sqrt([1,1;2,3]);
[~,K]       = size(alphas); [~,d] = size(mus);
r0          = chi2inv(0.8,d); r = [r0,r0];
sigs        = Comp_True_Sigmas(mus,taus,r,2);
nu          = 15; del = 0.1;
M1          = Compute_M_v2(alphas,mus,taus,r,del,d); 
samp        = Sample_MVTNM_DC(M1,alphas,mus,taus,r);
xi          = samp.mix;

out_KM      = KmeansPP_DC(xi,K,1);
mean(out_KM.labels~=samp.labels)*100
out         = MVTNM_DC_OptLL(samp,out_KM,r,nu);
alphas_hat  = out.alphas;
mus_hat     = out.mus;
taus_hat    = out.taus;
sigs_hat    = out.sigs;
mean(out.labels~=samp.labels)*100

clear r0 r nu del 

%% Test MV TNM-EM with known and estimated radii
alphas         = [1/3,1/3,1/3];
mus            = [3,3;0,0;-3,-3]; [K,d] = size(mus);
taus           = sqrt([1,1;2,3;1,1]);
r0             = chi2inv(0.6,d); r = [r0,r0,r0];
sigs           = Comp_True_Sigmas(mus,taus,r,d);
nu             = 15; del = 0.1;
M              = Compute_M_v2(alphas,mus,taus,r,del,d); 
samp           = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi             = samp.mix;

out_KM         = KmeansPP_DC(xi,K,1);
MCR_KM         = mean(out_KM.labels~=samp.labels)*100;
alphas_hat_KM  = out_KM.alphas;
mus_hat_KM     = out_KM.mus;
sigs_hat_KM    = out_KM.sigs;

out_EM1        = MVTNM_EM(xi,K,r,nu);
alphas_hat_EM1 = out_EM1.alphas;
mus_hat_EM1    = out_EM1.mus;
taus_hat_EM1   = out_EM1.taus;
sigs_hat_EM1   = out_EM1.sigs;
labels_EM1     = out_EM1.labels;
MCR_EM1        = mean(labels_EM1~=samp.labels)*100;

tstart = tic;
out_EM2        = MVTNM_EM_EstRadii(xi,K,nu,50);
telapse = toc(tstart);
alphas_hat_EM2 = out_EM2.alphas;
mus_hat_EM2    = out_EM2.mus;
taus_hat_EM2   = out_EM2.taus;
sigs_hat_EM2   = out_EM2.sigs;
labels_EM2     = out_EM2.labels;
MCR_EM2        = mean(labels_EM2~=samp.labels)*100;

del_bar     = 1-sqrt(1-0.1);
[alphas_bar,betas_bar,~,~]  = Comp_DRSP_Parameters(samp.comps,mus,sigs,M/2,del_bar);
meh         = Test_CR_MV(mus,sigs,mus_hat_KM,sigs_hat_KM,alphas_bar,betas_bar);
meh2        = Test_CR_MV(mus,sigs,mus_hat_EM1,sigs_hat_EM1,alphas_bar,betas_bar);
meh3        = Test_CR_MV(mus,sigs,mus_hat_EM2,sigs_hat_EM2,alphas_bar,betas_bar);

%clear M1 K d del r r0 samp xi t1