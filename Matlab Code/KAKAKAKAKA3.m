alphas  = [1/3,1/3,1/3]; taus  = sqrt([1,1;1,1;1,1]);
mus     = [3,3;0,-0.0;-3,3]; [K,d] = size(mus);
r0      = chi2inv(0.6,d); r = [r0,r0,r0]; 
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; 
M       = Compute_M_v2(alphas,mus,taus,r,del,d);  
samp    = Sample_MVTNM_DC(M,alphas,mus,taus,r);
xi      = samp.mix;

del2    = 1-(1-del)^(1/K);
M2      = Compute_M_v2(alphas,mus,taus,r,del2,d); 

%%
gridx1  = min(xi(:,1)):.1:max(xi(:,1));
gridx2  = min(xi(:,2)):.1:max(xi(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
x1 = x1(:); x2 = x2(:); x = [x1 x2];
figure(1)
ksdensity(xi,x);

%%
meh     = KmeansPP_DC(xi,K,1);
100*mean(meh.labels~=samp.labels)

meh2    = MVTNM_EM(samp,K,r,nu);
100*mean(meh2.labels~=samp.labels)

meh3   = MVTNM_EM_EstRadii(samp,K,nu,50);
100*mean(meh3.labels~=samp.labels)

[alphas_bar,betas_bar,~,~]  = Comp_DRSP_Parameters(samp.comps,mus,sigs,M/K,del_bar);
CR_Res  = Test_CR_MV(mus,sigs,meh.mus,meh.sigs,alphas_bar,betas_bar)
CR_Res2 = Test_CR_MV(mus,sigs,meh2.mus,meh2.sigs,alphas_bar,betas_bar)
CR_Res3 = Test_CR_MV(mus,sigs,meh3.mus,meh3.sigs,alphas_bar,betas_bar)
